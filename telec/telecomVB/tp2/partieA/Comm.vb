﻿Imports System.IO.Ports

'David Bernard
Public Class Comm
    Private DISABLED As Color = Color.Red


    Private port As MonPort

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim liste As New List(Of String)

        Me.port = New MonPort

        liste.AddRange(SerialPort.GetPortNames)
        ComboBox1.DataSource = liste
        Control.CheckForIllegalCrossThreadCalls = False

        AddHandler Me.port.PinChanged, AddressOf Me.ManagePinChange

        Me.btOpen.Enabled = True
        Me.btClose.Enabled = False

        Me.CommLabel.Text = ""
    End Sub

    Private Sub RefreshLed()

        If Me.port.IsOpen Then
            Try
                If Me.port.DtrEnable Then
                    Me.tbDTR.BackColor = Color.Green
                Else
                    Me.tbDTR.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbDTR.BackColor = Color.Yellow
            End Try


            Try
                If Me.port.CDHolding Then
                    Me.tbCD.BackColor = Color.Green
                Else
                    Me.tbCD.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbCD.BackColor = Color.Yellow
            End Try


            Try
                If Me.port.CtsHolding Then
                    Me.tbCTS.BackColor = Color.Green
                Else
                    Me.tbCTS.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbCD.BackColor = Color.Yellow
            End Try


            Try

                If Me.port.RtsEnable Then
                    Me.tbRTS.BackColor = Color.Green
                Else
                    Me.tbRTS.BackColor = Color.Red
                End If

            Catch ex As Exception
                Me.tbCD.BackColor = Color.Yellow
            End Try

            Try
                If Me.port.DsrHolding Then
                    Me.tbDSR.BackColor = Color.Green
                Else
                    Me.tbDSR.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbCD.BackColor = Color.Yellow
            End Try
        Else
            Me.tbCD.BackColor = Color.Yellow
            Me.tbDSR.BackColor = Color.Yellow
            Me.tbDTR.BackColor = Color.Yellow
            Me.tbCTS.BackColor = Color.Yellow
            Me.tbRTS.BackColor = Color.Yellow
        End If

    End Sub

    Private Sub SetToUnknwon()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btOpen.Click
        Dim comm As String


        If Not Me.port.IsOpen Then
            comm = Me.ComboBox1.SelectedItem
            Me.port.PortName = comm
            Me.port.Open()
            Me.RefreshLed()

            Me.CommLabel.Text = comm
            Me.Text = comm
            Me.btOpen.Enabled = False
            Me.btClose.Enabled = True
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btClose.Click

        If Me.port.IsOpen Then
            Me.port.Close()
            Me.RefreshLed()



            Me.CommLabel.Text = ""
            Me.Text = ""
            Me.btOpen.Enabled = True
            Me.btClose.Enabled = False
        End If
    End Sub

    Private Sub ManagePinChange()

        Me.RefreshLed()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub btRTS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRTS.Click
        Me.port.RtsEnable = Not Me.port.RtsEnable
        Me.RefreshLed()
    End Sub

    Private Sub btDRT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDRT.Click
        Me.port.DtrEnable = Not Me.port.DtrEnable
        Me.RefreshLed()
    End Sub
End Class
