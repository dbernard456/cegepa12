﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Comm
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.btOpen = New System.Windows.Forms.Button()
        Me.btClose = New System.Windows.Forms.Button()
        Me.tbDTR = New System.Windows.Forms.TextBox()
        Me.tbDSR = New System.Windows.Forms.TextBox()
        Me.tbCD = New System.Windows.Forms.TextBox()
        Me.tbRTS = New System.Windows.Forms.TextBox()
        Me.tbCTS = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btDRT = New System.Windows.Forms.Button()
        Me.btRTS = New System.Windows.Forms.Button()
        Me.CommLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(12, 25)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(208, 21)
        Me.ComboBox1.TabIndex = 0
        '
        'btOpen
        '
        Me.btOpen.Location = New System.Drawing.Point(11, 166)
        Me.btOpen.Name = "btOpen"
        Me.btOpen.Size = New System.Drawing.Size(75, 23)
        Me.btOpen.TabIndex = 1
        Me.btOpen.Text = "Ouvrir"
        Me.btOpen.UseVisualStyleBackColor = True
        '
        'btClose
        '
        Me.btClose.Location = New System.Drawing.Point(92, 166)
        Me.btClose.Name = "btClose"
        Me.btClose.Size = New System.Drawing.Size(75, 23)
        Me.btClose.TabIndex = 2
        Me.btClose.Text = "Fermer"
        Me.btClose.UseVisualStyleBackColor = True
        '
        'tbDTR
        '
        Me.tbDTR.BackColor = System.Drawing.Color.Yellow
        Me.tbDTR.Location = New System.Drawing.Point(14, 99)
        Me.tbDTR.Name = "tbDTR"
        Me.tbDTR.ReadOnly = True
        Me.tbDTR.Size = New System.Drawing.Size(100, 20)
        Me.tbDTR.TabIndex = 3
        Me.tbDTR.TabStop = False
        Me.tbDTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbDSR
        '
        Me.tbDSR.BackColor = System.Drawing.Color.Yellow
        Me.tbDSR.Location = New System.Drawing.Point(257, 48)
        Me.tbDSR.Name = "tbDSR"
        Me.tbDSR.ReadOnly = True
        Me.tbDSR.Size = New System.Drawing.Size(100, 20)
        Me.tbDSR.TabIndex = 4
        Me.tbDSR.TabStop = False
        Me.tbDSR.Text = "DSR"
        Me.tbDSR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbCD
        '
        Me.tbCD.BackColor = System.Drawing.Color.Yellow
        Me.tbCD.Location = New System.Drawing.Point(257, 74)
        Me.tbCD.Name = "tbCD"
        Me.tbCD.ReadOnly = True
        Me.tbCD.Size = New System.Drawing.Size(100, 20)
        Me.tbCD.TabIndex = 5
        Me.tbCD.TabStop = False
        Me.tbCD.Text = "CD"
        Me.tbCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbRTS
        '
        Me.tbRTS.BackColor = System.Drawing.Color.Yellow
        Me.tbRTS.Location = New System.Drawing.Point(120, 99)
        Me.tbRTS.Name = "tbRTS"
        Me.tbRTS.ReadOnly = True
        Me.tbRTS.Size = New System.Drawing.Size(100, 20)
        Me.tbRTS.TabIndex = 6
        Me.tbRTS.TabStop = False
        Me.tbRTS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbCTS
        '
        Me.tbCTS.BackColor = System.Drawing.Color.Yellow
        Me.tbCTS.Location = New System.Drawing.Point(257, 100)
        Me.tbCTS.Name = "tbCTS"
        Me.tbCTS.ReadOnly = True
        Me.tbCTS.Size = New System.Drawing.Size(100, 20)
        Me.tbCTS.TabIndex = 7
        Me.tbCTS.TabStop = False
        Me.tbCTS.Text = "CTS"
        Me.tbCTS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Port de communication"
        '
        'btDRT
        '
        Me.btDRT.Location = New System.Drawing.Point(15, 70)
        Me.btDRT.Name = "btDRT"
        Me.btDRT.Size = New System.Drawing.Size(99, 23)
        Me.btDRT.TabIndex = 9
        Me.btDRT.Text = "DRT"
        Me.btDRT.UseVisualStyleBackColor = True
        '
        'btRTS
        '
        Me.btRTS.Location = New System.Drawing.Point(120, 70)
        Me.btRTS.Name = "btRTS"
        Me.btRTS.Size = New System.Drawing.Size(100, 23)
        Me.btRTS.TabIndex = 10
        Me.btRTS.Text = "RTS"
        Me.btRTS.UseVisualStyleBackColor = True
        '
        'CommLabel
        '
        Me.CommLabel.AutoSize = True
        Me.CommLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CommLabel.Location = New System.Drawing.Point(190, 156)
        Me.CommLabel.Name = "CommLabel"
        Me.CommLabel.Size = New System.Drawing.Size(99, 31)
        Me.CommLabel.TabIndex = 11
        Me.CommLabel.Text = "COMM"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(391, 218)
        Me.Controls.Add(Me.CommLabel)
        Me.Controls.Add(Me.btRTS)
        Me.Controls.Add(Me.btDRT)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbCTS)
        Me.Controls.Add(Me.tbRTS)
        Me.Controls.Add(Me.tbCD)
        Me.Controls.Add(Me.tbDSR)
        Me.Controls.Add(Me.tbDTR)
        Me.Controls.Add(Me.btClose)
        Me.Controls.Add(Me.btOpen)
        Me.Controls.Add(Me.ComboBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents btOpen As System.Windows.Forms.Button
    Friend WithEvents btClose As System.Windows.Forms.Button
    Friend WithEvents tbDTR As System.Windows.Forms.TextBox
    Friend WithEvents tbDSR As System.Windows.Forms.TextBox
    Friend WithEvents tbCD As System.Windows.Forms.TextBox
    Friend WithEvents tbRTS As System.Windows.Forms.TextBox
    Friend WithEvents tbCTS As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btDRT As System.Windows.Forms.Button
    Friend WithEvents btRTS As System.Windows.Forms.Button
    Friend WithEvents CommLabel As System.Windows.Forms.Label

End Class
