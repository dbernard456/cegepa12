﻿Imports System.IO.Ports

''' <summary>
''' Fenêtre permettant de configurer le port série
''' </summary>
''' <author>David Bernard et Nelson Kelly</author>
''' <remarks></remarks>
Public Class ConfigForm

    Private SpeedList As New List(Of String)
    Private DataList As New List(Of String)
    Private ParityList As New List(Of String)
    Private StopBitList As New List(Of String)
    Public Sub New()

        ' Cet appel est requis par le concepteur.
        InitializeComponent()

        ' Ajoutez une initialisation quelconque après l'appel InitializeComponent().
        Me.SpeedList.Add(1200)
        Me.SpeedList.Add(9600)

        Me.DataList.Add(7)
        Me.DataList.Add(8)

        Me.ParityList.Add("aucune")
        Me.ParityList.Add("paire")
        Me.ParityList.Add("impaire")

        Me.StopBitList.Add(1)
        Me.StopBitList.Add(2)

        Me.SpeedCb.DataSource = Me.SpeedList
        Me.DataCb.DataSource = Me.DataList
        Me.ParityCb.DataSource = Me.ParityList
        Me.StopBitCb.DataSource = Me.StopBitList

        Debug.Print("NEW")
    End Sub


    Private Sub ConfigForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Debug.Print("ConfigForm_Load")

    End Sub

    Private Sub ConfigForm_Activated(sender As System.Object, e As System.EventArgs) Handles MyBase.Activated
        Debug.Print("ConfigForm_Activated")
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Hide()
    End Sub

    Public Property Parity
        Get
            Dim selectedPar As String
            Dim output As Integer

            selectedPar = Me.ParityCb.SelectedItem

            If selectedPar = "paire" Then
                output = System.IO.Ports.Parity.Even
            ElseIf selectedPar = "impaire" Then
                output = System.IO.Ports.Parity.Odd
            Else
                output = System.IO.Ports.Parity.None
            End If

            Return output
        End Get
        Set(value)
            Dim selected As String

            If value = System.IO.Ports.Parity.Even Then
                selected = "paire"
            ElseIf System.IO.Ports.Parity.Odd Then
                selected = "impaire"
            Else
                selected = "aucune"
            End If

            Me.ParityCb.SelectedItem = selected
        End Set
    End Property

    Public Property BaudRate As Integer
        Get
            Return Me.SpeedCb.SelectedItem
        End Get
        Set(value As Integer)
            Me.SpeedCb.SelectedItem = value
        End Set
    End Property

    Public Property DataBits
        Get
            Return Me.DataCb.SelectedItem
        End Get
        Set(value)
            Me.DataCb.SelectedItem = value
        End Set
    End Property

    Public Property StopBits
        Get
            Return Me.StopBitCb.SelectedItem
        End Get
        Set(value)
            Me.StopBitCb.SelectedItem = value
        End Set
    End Property
End Class