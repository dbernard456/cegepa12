﻿Imports System.IO.Ports
Imports System.Windows

''' <summary>
''' Classe de l'interface Graphique Chat permettant de communiquer avec un autre post avec du rs232
''' </summary>
''' <author>David Bernard et Nelson Kelly</author>
''' <remarks></remarks>
Public Class Chat
    Private Const MAX_LINES As Integer = 10
    Private running As Boolean
    Private port As MonPort
    Private form As ConfigForm
    Private listeOutput As New List(Of String)
    Private listeError As New List(Of String)


    Private Sub Chat_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim liste As New List(Of String)

        Me.port = New MonPort

        AddHandler Me.port.DataReceived, AddressOf Me.DataReceivedHandler
        AddHandler Me.port.Connected, AddressOf Me.ConnectedHandler
        AddHandler Me.port.Disconnected, AddressOf Me.DisconnectedHandler
        AddHandler Me.port.PinChanged, AddressOf Me.ManagePinChange
        AddHandler Me.port.ErrorReceived, AddressOf Me.ErrorReceivedHandler

        liste.AddRange(SerialPort.GetPortNames)
        ComboBox1.DataSource = liste
        Control.CheckForIllegalCrossThreadCalls = False
        Me.InputBox.ReadOnly = True

        Me.CloseBtn.Enabled = False
        Me.sendBtn.Enabled = False
        Me.ComboBox1.Enabled = True

        form = New ConfigForm
        Me.form.Hide()

        RefreshConfig()
        Me.RefreshLed()

        Debug.Print("Chat_Load")


    End Sub

    Private Sub ManagePinChange()
        Me.RefreshLed()
    End Sub

    Private Sub InputBox_KeyUp(sender As System.Object, e As Forms.KeyEventArgs) Handles InputBox.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.SendData()
        End If
    End Sub

    Private Sub ConnectBtn_Click(sender As System.Object, e As System.EventArgs) Handles ConnectBtn.Click
        If Not Me.port.IsOpen Then
            Me.port.PortName = Me.ComboBox1.SelectedItem
            Me.port.Open()
        End If
    End Sub

    Private Sub CloseBtn_Click(sender As System.Object, e As System.EventArgs) Handles CloseBtn.Click
        If Me.port.IsOpen Then
            Me.port.Close()
        End If
    End Sub



    Private Sub ConnectedHandler(portName As String)
        Debug.Print("connected at " + portName)

        Me.CommLabel.Text = portName
        Me.InputBox.ReadOnly = False
        Me.sendBtn.Enabled = True
        Me.CloseBtn.Enabled = True
        Me.ConnectBtn.Enabled = False
        Me.ComboBox1.Enabled = False
        Me.ConfigBtn.Enabled = False

        Me.RefreshLed()
    End Sub

    Private Sub DisconnectedHandler()
        Debug.Print("disconnected")

        Me.CommLabel.Text = ""
        Me.InputBox.ReadOnly = True
        Me.sendBtn.Enabled = False
        Me.CloseBtn.Enabled = False
        Me.ConnectBtn.Enabled = True
        Me.ComboBox1.Enabled = True
        Me.ConfigBtn.Enabled = True

        Me.RefreshLed()
    End Sub

    Private Sub SendData()
        Dim inputText As String

        inputText = Me.InputBox.Text
        Debug.Print("Sending : " + inputText)

        Me.InputBox.Text = ""

        Me.port.WriteLine(inputText)
        Me.InputBox.Focus()

    End Sub

    Private Sub sendBtn_Click(sender As System.Object, e As System.EventArgs) Handles sendBtn.Click
        Me.SendData()
    End Sub

    Private Sub ConfigBtn_Click(sender As System.Object, e As System.EventArgs) Handles ConfigBtn.Click

        Me.form.ShowDialog()
        Me.RefreshConfig()
    End Sub

    Private Sub RefreshConfig()
        Me.port.Parity = Me.form.Parity
        Me.port.BaudRate = Me.form.BaudRate
        Me.port.StopBits = Me.form.StopBits
        Me.port.DataBits = Me.form.DataBits

        Me.ParityTb.Text = Me.port.Parity
        Me.BaudTb.Text = Me.port.BaudRate
        Me.StopBitsTb.Text = Me.port.StopBits
        Me.DataBitsTb.Text = Me.port.DataBits
    End Sub

    Private Sub RefreshLed()

        If Me.port.IsOpen Then
            Try
                If Me.port.DtrEnable Then
                    Me.tbDTR.BackColor = Color.Green
                Else
                    Me.tbDTR.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbDTR.BackColor = Color.Yellow
            End Try

            Try
                If Me.port.CDHolding Then
                    Me.tbCD.BackColor = Color.Green
                Else
                    Me.tbCD.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbCD.BackColor = Color.Yellow
            End Try

            Try
                If Me.port.CtsHolding Then
                    Me.tbCTS.BackColor = Color.Green
                Else
                    Me.tbCTS.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbCD.BackColor = Color.Yellow
            End Try


            Try
                If Me.port.RtsEnable Then
                    Me.tbRTS.BackColor = Color.Green
                Else
                    Me.tbRTS.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbCD.BackColor = Color.Yellow
            End Try

            Try
                If Me.port.DsrHolding Then
                    Me.tbDSR.BackColor = Color.Green
                Else
                    Me.tbDSR.BackColor = Color.Red
                End If
            Catch ex As Exception
                Me.tbCD.BackColor = Color.Yellow
            End Try
        Else
            Me.tbCD.BackColor = Color.Yellow
            Me.tbDSR.BackColor = Color.Yellow
            Me.tbDTR.BackColor = Color.Yellow
            Me.tbCTS.BackColor = Color.Yellow
            Me.tbRTS.BackColor = Color.Yellow
        End If
    End Sub

    Private Sub refreshBoxes()
        Me.OutputBox.Text = ""
        For Each s As String In Me.listeOutput
            Me.OutputBox.Text = Me.OutputBox.Text + s + vbNewLine
        Next

        Me.ErrorBox.Text = ""
        For Each s As String In Me.listeError
            Me.ErrorBox.Text = Me.ErrorBox.Text + s + vbNewLine
        Next
    End Sub

    Private Sub DataReceivedHandler(text As String)
        Chat.AddToList(Me.listeOutput, text, Chat.MAX_LINES)
        Me.refreshBoxes()
    End Sub

    Private Sub ErrorReceivedHandler(text As String)
        Chat.AddToList(Me.listeError, text, Chat.MAX_LINES)
        Me.refreshBoxes()
    End Sub



    Private Sub btRTS_Click(sender As System.Object, e As System.EventArgs) Handles btRTS.Click
        Me.port.RtsEnable = Not Me.port.RtsEnable
        Me.RefreshLed()
    End Sub

    Private Sub btDRT_Click(sender As System.Object, e As System.EventArgs) Handles btDRT.Click
        Me.port.DtrEnable = Not Me.port.DtrEnable
        Me.RefreshLed()
    End Sub



    Private Shared Sub AddToList(liste As List(Of String), text As String, maxItem As Integer)
        Dim itemCount As Integer

        itemCount = liste.Count

        If itemCount > maxItem Then
            liste.RemoveAt(itemCount - 1)
        End If

        liste.Insert(0, text)
    End Sub
End Class