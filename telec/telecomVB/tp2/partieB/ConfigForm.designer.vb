﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfigForm
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SpeedCb = New System.Windows.Forms.ComboBox()
        Me.DataCb = New System.Windows.Forms.ComboBox()
        Me.ParityCb = New System.Windows.Forms.ComboBox()
        Me.StopBitCb = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(220, 229)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "OK"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'SpeedCb
        '
        Me.SpeedCb.FormattingEnabled = True
        Me.SpeedCb.Location = New System.Drawing.Point(47, 52)
        Me.SpeedCb.Name = "SpeedCb"
        Me.SpeedCb.Size = New System.Drawing.Size(248, 21)
        Me.SpeedCb.TabIndex = 2
        '
        'DataCb
        '
        Me.DataCb.FormattingEnabled = True
        Me.DataCb.Location = New System.Drawing.Point(48, 92)
        Me.DataCb.Name = "DataCb"
        Me.DataCb.Size = New System.Drawing.Size(247, 21)
        Me.DataCb.TabIndex = 3
        '
        'ParityCb
        '
        Me.ParityCb.FormattingEnabled = True
        Me.ParityCb.Location = New System.Drawing.Point(47, 135)
        Me.ParityCb.Name = "ParityCb"
        Me.ParityCb.Size = New System.Drawing.Size(248, 21)
        Me.ParityCb.TabIndex = 4
        '
        'StopBitCb
        '
        Me.StopBitCb.FormattingEnabled = True
        Me.StopBitCb.Location = New System.Drawing.Point(47, 178)
        Me.StopBitCb.Name = "StopBitCb"
        Me.StopBitCb.Size = New System.Drawing.Size(248, 21)
        Me.StopBitCb.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(47, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Vitesse"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(47, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Donnees"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(47, 116)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Parité"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(47, 159)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Bits arret"
        '
        'ConfigForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(355, 285)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.StopBitCb)
        Me.Controls.Add(Me.ParityCb)
        Me.Controls.Add(Me.DataCb)
        Me.Controls.Add(Me.SpeedCb)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "ConfigForm"
        Me.Text = "Config"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents SpeedCb As System.Windows.Forms.ComboBox
    Friend WithEvents DataCb As System.Windows.Forms.ComboBox
    Friend WithEvents ParityCb As System.Windows.Forms.ComboBox
    Friend WithEvents StopBitCb As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
