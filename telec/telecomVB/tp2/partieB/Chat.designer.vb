﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Chat
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OutputBox = New System.Windows.Forms.RichTextBox()
        Me.InputBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ErrorBox = New System.Windows.Forms.RichTextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ConnectBtn = New System.Windows.Forms.Button()
        Me.CloseBtn = New System.Windows.Forms.Button()
        Me.CommLabel = New System.Windows.Forms.Label()
        Me.sendBtn = New System.Windows.Forms.Button()
        Me.ConfigBtn = New System.Windows.Forms.Button()
        Me.btRTS = New System.Windows.Forms.Button()
        Me.btDRT = New System.Windows.Forms.Button()
        Me.tbCTS = New System.Windows.Forms.TextBox()
        Me.tbRTS = New System.Windows.Forms.TextBox()
        Me.tbCD = New System.Windows.Forms.TextBox()
        Me.tbDSR = New System.Windows.Forms.TextBox()
        Me.tbDTR = New System.Windows.Forms.TextBox()
        Me.BaudTb = New System.Windows.Forms.TextBox()
        Me.DataBitsTb = New System.Windows.Forms.TextBox()
        Me.StopBitsTb = New System.Windows.Forms.TextBox()
        Me.ParityTb = New System.Windows.Forms.TextBox()
        Me.Vitesse = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'OutputBox
        '
        Me.OutputBox.Location = New System.Drawing.Point(12, 64)
        Me.OutputBox.Name = "OutputBox"
        Me.OutputBox.ReadOnly = True
        Me.OutputBox.Size = New System.Drawing.Size(715, 197)
        Me.OutputBox.TabIndex = 0
        Me.OutputBox.TabStop = False
        Me.OutputBox.Text = ""
        '
        'InputBox
        '
        Me.InputBox.Location = New System.Drawing.Point(12, 25)
        Me.InputBox.Name = "InputBox"
        Me.InputBox.Size = New System.Drawing.Size(715, 20)
        Me.InputBox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Input"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Output"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 264)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Error"
        '
        'ErrorBox
        '
        Me.ErrorBox.Location = New System.Drawing.Point(15, 280)
        Me.ErrorBox.Name = "ErrorBox"
        Me.ErrorBox.ReadOnly = True
        Me.ErrorBox.Size = New System.Drawing.Size(715, 199)
        Me.ErrorBox.TabIndex = 5
        Me.ErrorBox.TabStop = False
        Me.ErrorBox.Text = ""
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(18, 485)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(199, 21)
        Me.ComboBox1.TabIndex = 6
        '
        'ConnectBtn
        '
        Me.ConnectBtn.Location = New System.Drawing.Point(224, 485)
        Me.ConnectBtn.Name = "ConnectBtn"
        Me.ConnectBtn.Size = New System.Drawing.Size(75, 23)
        Me.ConnectBtn.TabIndex = 7
        Me.ConnectBtn.TabStop = False
        Me.ConnectBtn.Text = "Connect"
        Me.ConnectBtn.UseVisualStyleBackColor = True
        '
        'CloseBtn
        '
        Me.CloseBtn.Location = New System.Drawing.Point(306, 485)
        Me.CloseBtn.Name = "CloseBtn"
        Me.CloseBtn.Size = New System.Drawing.Size(75, 23)
        Me.CloseBtn.TabIndex = 8
        Me.CloseBtn.TabStop = False
        Me.CloseBtn.Text = "Close"
        Me.CloseBtn.UseVisualStyleBackColor = True
        '
        'CommLabel
        '
        Me.CommLabel.AutoSize = True
        Me.CommLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CommLabel.Location = New System.Drawing.Point(468, 487)
        Me.CommLabel.Name = "CommLabel"
        Me.CommLabel.Size = New System.Drawing.Size(0, 26)
        Me.CommLabel.TabIndex = 9
        '
        'sendBtn
        '
        Me.sendBtn.Location = New System.Drawing.Point(657, 490)
        Me.sendBtn.Name = "sendBtn"
        Me.sendBtn.Size = New System.Drawing.Size(75, 23)
        Me.sendBtn.TabIndex = 10
        Me.sendBtn.TabStop = False
        Me.sendBtn.Text = "Send"
        Me.sendBtn.UseVisualStyleBackColor = True
        '
        'ConfigBtn
        '
        Me.ConfigBtn.Location = New System.Drawing.Point(387, 485)
        Me.ConfigBtn.Name = "ConfigBtn"
        Me.ConfigBtn.Size = New System.Drawing.Size(75, 23)
        Me.ConfigBtn.TabIndex = 11
        Me.ConfigBtn.TabStop = False
        Me.ConfigBtn.Text = "Config"
        Me.ConfigBtn.UseVisualStyleBackColor = True
        '
        'btRTS
        '
        Me.btRTS.Location = New System.Drawing.Point(17, 534)
        Me.btRTS.Name = "btRTS"
        Me.btRTS.Size = New System.Drawing.Size(100, 23)
        Me.btRTS.TabIndex = 18
        Me.btRTS.Text = "RTS"
        Me.btRTS.UseVisualStyleBackColor = True
        '
        'btDRT
        '
        Me.btDRT.Location = New System.Drawing.Point(123, 534)
        Me.btDRT.Name = "btDRT"
        Me.btDRT.Size = New System.Drawing.Size(99, 23)
        Me.btDRT.TabIndex = 17
        Me.btDRT.Text = "DRT"
        Me.btDRT.UseVisualStyleBackColor = True
        '
        'tbCTS
        '
        Me.tbCTS.BackColor = System.Drawing.Color.Yellow
        Me.tbCTS.Location = New System.Drawing.Point(236, 563)
        Me.tbCTS.Name = "tbCTS"
        Me.tbCTS.ReadOnly = True
        Me.tbCTS.Size = New System.Drawing.Size(100, 20)
        Me.tbCTS.TabIndex = 16
        Me.tbCTS.TabStop = False
        Me.tbCTS.Text = "CTS"
        Me.tbCTS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbRTS
        '
        Me.tbRTS.BackColor = System.Drawing.Color.Yellow
        Me.tbRTS.Location = New System.Drawing.Point(17, 563)
        Me.tbRTS.Name = "tbRTS"
        Me.tbRTS.ReadOnly = True
        Me.tbRTS.Size = New System.Drawing.Size(100, 20)
        Me.tbRTS.TabIndex = 15
        Me.tbRTS.TabStop = False
        Me.tbRTS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbCD
        '
        Me.tbCD.BackColor = System.Drawing.Color.Yellow
        Me.tbCD.Location = New System.Drawing.Point(236, 537)
        Me.tbCD.Name = "tbCD"
        Me.tbCD.ReadOnly = True
        Me.tbCD.Size = New System.Drawing.Size(100, 20)
        Me.tbCD.TabIndex = 14
        Me.tbCD.TabStop = False
        Me.tbCD.Text = "CD"
        Me.tbCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbDSR
        '
        Me.tbDSR.BackColor = System.Drawing.Color.Yellow
        Me.tbDSR.Location = New System.Drawing.Point(236, 589)
        Me.tbDSR.Name = "tbDSR"
        Me.tbDSR.ReadOnly = True
        Me.tbDSR.Size = New System.Drawing.Size(100, 20)
        Me.tbDSR.TabIndex = 13
        Me.tbDSR.TabStop = False
        Me.tbDSR.Text = "DSR"
        Me.tbDSR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbDTR
        '
        Me.tbDTR.BackColor = System.Drawing.Color.Yellow
        Me.tbDTR.Location = New System.Drawing.Point(122, 563)
        Me.tbDTR.Name = "tbDTR"
        Me.tbDTR.ReadOnly = True
        Me.tbDTR.Size = New System.Drawing.Size(100, 20)
        Me.tbDTR.TabIndex = 12
        Me.tbDTR.TabStop = False
        Me.tbDTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BaudTb
        '
        Me.BaudTb.Location = New System.Drawing.Point(632, 536)
        Me.BaudTb.Name = "BaudTb"
        Me.BaudTb.ReadOnly = True
        Me.BaudTb.Size = New System.Drawing.Size(100, 20)
        Me.BaudTb.TabIndex = 19
        Me.BaudTb.TabStop = False
        '
        'DataBitsTb
        '
        Me.DataBitsTb.Location = New System.Drawing.Point(632, 562)
        Me.DataBitsTb.Name = "DataBitsTb"
        Me.DataBitsTb.ReadOnly = True
        Me.DataBitsTb.Size = New System.Drawing.Size(100, 20)
        Me.DataBitsTb.TabIndex = 20
        Me.DataBitsTb.TabStop = False
        '
        'StopBitsTb
        '
        Me.StopBitsTb.Location = New System.Drawing.Point(632, 614)
        Me.StopBitsTb.Name = "StopBitsTb"
        Me.StopBitsTb.ReadOnly = True
        Me.StopBitsTb.Size = New System.Drawing.Size(100, 20)
        Me.StopBitsTb.TabIndex = 21
        Me.StopBitsTb.TabStop = False
        '
        'ParityTb
        '
        Me.ParityTb.Location = New System.Drawing.Point(632, 588)
        Me.ParityTb.Name = "ParityTb"
        Me.ParityTb.ReadOnly = True
        Me.ParityTb.Size = New System.Drawing.Size(100, 20)
        Me.ParityTb.TabIndex = 22
        Me.ParityTb.TabStop = False
        '
        'Vitesse
        '
        Me.Vitesse.AutoSize = True
        Me.Vitesse.Location = New System.Drawing.Point(585, 543)
        Me.Vitesse.Name = "Vitesse"
        Me.Vitesse.Size = New System.Drawing.Size(41, 13)
        Me.Vitesse.TabIndex = 23
        Me.Vitesse.Text = "Vitesse"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(581, 569)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Data Bit"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(593, 595)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Parity"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(583, 617)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Stop bit"
        '
        'Chat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(786, 650)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Vitesse)
        Me.Controls.Add(Me.ParityTb)
        Me.Controls.Add(Me.StopBitsTb)
        Me.Controls.Add(Me.DataBitsTb)
        Me.Controls.Add(Me.BaudTb)
        Me.Controls.Add(Me.btRTS)
        Me.Controls.Add(Me.btDRT)
        Me.Controls.Add(Me.tbCTS)
        Me.Controls.Add(Me.tbRTS)
        Me.Controls.Add(Me.tbCD)
        Me.Controls.Add(Me.tbDSR)
        Me.Controls.Add(Me.tbDTR)
        Me.Controls.Add(Me.ConfigBtn)
        Me.Controls.Add(Me.sendBtn)
        Me.Controls.Add(Me.CommLabel)
        Me.Controls.Add(Me.CloseBtn)
        Me.Controls.Add(Me.ConnectBtn)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.ErrorBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.InputBox)
        Me.Controls.Add(Me.OutputBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "Chat"
        Me.Text = "Chat - David Bernard & Nelson Kelly - TP2b"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OutputBox As System.Windows.Forms.RichTextBox
    Friend WithEvents InputBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ErrorBox As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ConnectBtn As System.Windows.Forms.Button
    Friend WithEvents CloseBtn As System.Windows.Forms.Button
    Friend WithEvents CommLabel As System.Windows.Forms.Label
    Friend WithEvents sendBtn As System.Windows.Forms.Button
    Friend WithEvents ConfigBtn As System.Windows.Forms.Button
    Friend WithEvents btRTS As System.Windows.Forms.Button
    Friend WithEvents btDRT As System.Windows.Forms.Button
    Friend WithEvents tbCTS As System.Windows.Forms.TextBox
    Friend WithEvents tbRTS As System.Windows.Forms.TextBox
    Friend WithEvents tbCD As System.Windows.Forms.TextBox
    Friend WithEvents tbDSR As System.Windows.Forms.TextBox
    Friend WithEvents tbDTR As System.Windows.Forms.TextBox
    Friend WithEvents BaudTb As System.Windows.Forms.TextBox
    Friend WithEvents DataBitsTb As System.Windows.Forms.TextBox
    Friend WithEvents StopBitsTb As System.Windows.Forms.TextBox
    Friend WithEvents ParityTb As System.Windows.Forms.TextBox
    Friend WithEvents Vitesse As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
