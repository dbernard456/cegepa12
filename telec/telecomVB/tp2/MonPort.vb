﻿Imports System.IO.Ports

''' <summary>
''' Classe qui encapsule le port série
''' </summary>
''' <author>David Bernard et Nelson Kelly</author>
''' <remarks></remarks>
Public Class MonPort
    Private port As SerialPort

    Public Event PinChanged()
    Public Event DataReceived(text As String)
    Public Event ErrorReceived(text As String)
    Public Event Connected(comm As String)
    Public Event Disconnected()


    Public Sub New()
        Me.port = New SerialPort

        AddHandler Me.port.PinChanged, AddressOf Me.ManagePinChanged

        AddHandler Me.port.DataReceived, AddressOf Me.DataReceivedHandler
        AddHandler Me.port.ErrorReceived, AddressOf Me.ErrorReceivedHandler

        Me.port.Parity = Parity.None
        Me.port.BaudRate = 9600
        Me.port.StopBits = StopBits.Two
        Me.port.DataBits = 8

    End Sub

    Public Property PortName As String
        Get
            Return Me.port.PortName
        End Get
        Set(value As String)
            Me.port.PortName = value
        End Set
    End Property

    Public ReadOnly Property DsrHolding As Boolean
        Get
            Return Me.port.DsrHolding
        End Get
    End Property

    Public Property DtrEnable As Boolean
        Get
            Return Me.port.DtrEnable
        End Get
        Set(value As Boolean)
            Me.port.DtrEnable = value
        End Set
    End Property

    Public ReadOnly Property CDHolding As Boolean
        Get
            Return Me.port.CDHolding
        End Get
    End Property

    Public Property RtsEnable As Boolean
        Get
            Return Me.port.RtsEnable
        End Get
        Set(value As Boolean)
            Me.port.RtsEnable = value
        End Set
    End Property

    Public ReadOnly Property CtsHolding As Boolean
        Get
            Return Me.port.CtsHolding
        End Get
    End Property

    Public ReadOnly Property IsOpen
        Get
            Return Me.port.IsOpen
        End Get
    End Property

    Public Property BaudRate
        Get
            Return Me.port.BaudRate
        End Get
        Set(value)
            Me.port.BaudRate = value
        End Set
    End Property

    Public Property Parity
        Get
            Return Me.port.Parity
        End Get
        Set(value)
            Me.port.Parity = value
        End Set
    End Property

    Public Property StopBits
        Get
            Return Me.port.StopBits
        End Get
        Set(value)
            Me.port.StopBits = value
        End Set
    End Property

    Public Property DataBits
        Get
            Return Me.port.DataBits
        End Get
        Set(value)
            Me.port.DataBits = value
        End Set
    End Property

    Private Sub ManagePinChanged()

        RaiseEvent PinChanged()
    End Sub

    Public Sub Open()
        Try
            Me.port.Open()
            RaiseEvent Connected(Me.PortName)
        Catch ex As Exception
            RaiseEvent ErrorReceived("Openning error")
        End Try
    End Sub

    Public Sub Close()
        Me.port.Close()
        RaiseEvent Disconnected()
    End Sub

    Private Sub DataReceivedHandler(sender As Object, e As SerialDataReceivedEventArgs)
        RaiseEvent DataReceived(Me.port.ReadLine())
    End Sub

    Private Sub ErrorReceivedHandler(sender As Object, e As SerialErrorReceivedEventArgs)


        RaiseEvent ErrorReceived(e.EventType.ToString())
    End Sub

    Public Sub WriteLine(text As String)
        Me.port.WriteLine(text)
    End Sub
End Class
