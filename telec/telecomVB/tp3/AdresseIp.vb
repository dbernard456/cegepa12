﻿
''' <summary>
''' Classe qui représente un adresse ipv4.
''' </summary>
''' <remarks>
''' David Bernard
''' </remarks>
Public Class AdresseIp

    Private Const SUBNETWORK_MASK_CELLING As UInt32 = 30
    Private ReadOnly _ipString As String
    Private ReadOnly _classe As IpClass
    Private ReadOnly _ip As UInt32
    Private ReadOnly _mask As UInt32
    Private ReadOnly _maskNB As UInt32
    Private ReadOnly _broadcast As UInt32

    Enum IpClass
        CLASS_A = 8
        CLASS_B = 16
        CLASS_C = 24
        OTHER = -1
    End Enum

    Public Sub New(ByVal ipString As String, ByVal subNetworks As UInt32)
        Dim tempMaskNb As UInt32

        ' Le nombre minimun de sous réseau est de 1
        If (subNetworks < 1) Then
            Throw New Exception
        End If

        Me._ipString = ipString

        ' conversion en entier
        Me._ip = AdresseIp.ipStringToInt(ipString)

        ' calcul de la classe
        Me._classe = AdresseIp.CalculateClass(Me._ip)

        If Me._classe = IpClass.OTHER Then
            ' Ne devrais jamais arrivé, dans l'éventualité, il est préférable de lancé une exception que d'assigné un faux masque.
            Throw New Exception
        Else
            tempMaskNb = Me._classe
        End If

        ' Calculer le nombre de bits pour le masque de sous-réseau
        Me._maskNB = tempMaskNb + subNetworks - 1

        If Me._maskNB > AdresseIp.SUBNETWORK_MASK_CELLING Then
            Throw New Exception
        End If

        ' Construire le masque de sous-réseau
        Me._mask = CreateMask(Me._maskNB)

        ' Calculer l'adresse de broadcast
        Me._broadcast = AdresseIp.CalculateBroadcast(Me._ip, Me._mask)

    End Sub

#Region "Properties"
    Public ReadOnly Property Classe As String
        Get
            If Me.IsClassA Then
                Return "A"
            ElseIf Me.IsClassB Then
                Return "B"
            ElseIf Me.IsClassC Then
                Return "C"
            Else
                Return "?"
            End If
        End Get
    End Property

    Public ReadOnly Property Ip As UInt32
        Get
            Return Me._ip
        End Get
    End Property

    Public ReadOnly Property Broadcast As UInt32
        Get
            Return Me._broadcast
        End Get
    End Property

    Public ReadOnly Property Mask As UInt32
        Get
            Return Me._mask
        End Get
    End Property

    Public ReadOnly Property MaskNb As UInt32
        Get
            Return Me._maskNB
        End Get
    End Property

    Public ReadOnly Property IpString As String
        Get
            Return AdresseIp.intToIpString(Me.Ip)
        End Get
    End Property

    Public ReadOnly Property BroadcastString As String
        Get
            Return AdresseIp.intToIpString(Me.Broadcast)
        End Get
    End Property

    Public ReadOnly Property MaskString As String
        Get
            Return AdresseIp.intToIpString(Me.Mask)
        End Get
    End Property

    Public ReadOnly Property IsClassA As Boolean
        Get
            Return Me._classe = IpClass.CLASS_A
        End Get
    End Property


    Public ReadOnly Property IsClassB As Boolean
        Get
            Return Me._classe = IpClass.CLASS_B
        End Get
    End Property

    Public ReadOnly Property IsClassC As Boolean
        Get
            Return Me._classe = IpClass.CLASS_C
        End Get
    End Property


    Public ReadOnly Property NbHost As UInt32
        Get
            Return (Not Me.Mask) - 1
        End Get
    End Property

    Public ReadOnly Property NbSubNetwork As UInt32
        Get
            Return 0
        End Get
    End Property
#End Region

#Region "Functions"
    Public Shared Function intToIpString(ByVal ip As UInt32) As String
        Dim output As String
        Dim buffer As String

        output = ""

        ' les bits 0 à 7 en partant de la gauche
        buffer = ip And &HFF000000&
        buffer = buffer >> 24
        output = output & buffer & "."

        ' les bits 8 à 15 en partant de la gauche
        buffer = ip And &HFF0000&
        buffer = buffer >> 16
        output = output & buffer & "."

        ' les bits 16 à 23 en partant de la gauche
        buffer = ip And &HFF00&
        buffer = buffer >> 8
        output = output & buffer & "."

        ' les bits 24 à 32 en partant de la gauche
        buffer = ip And &HFF&
        output = output & buffer

        Return output
    End Function

    Public Shared Function ipStringToInt(ByVal ipString As String) As UInt32
        Dim shiftCourant As UInt32 = 32
        Dim buffer As UInt32 = 0
        Dim ipFinal As UInt32 = 0

        For Each partie As String In Split(ipString, ".")
            buffer = Integer.Parse(partie)

            If (buffer < 0) Or (buffer > 255) Then
                Throw New Exception
            End If

            shiftCourant = shiftCourant - 8
            buffer = buffer << shiftCourant

            ipFinal = ipFinal Or buffer
        Next

        Return ipFinal
    End Function

    Public Shared Function CalculateBroadcast(ByVal ip As UInt32, ByVal mask As UInt32) As UInt32
        Dim buffer As UInt32

        buffer = Not mask
        buffer = ip Or buffer

        Return buffer
    End Function

    Private Shared Function CreateMask(ByVal bits As UInt32)
        Dim result As UInt32
        Dim mask As UInt32

        mask = &H80000000&
        result = 0

        While (bits > 0)
            result = result Or mask
            mask = mask >> 1
            bits = bits - 1
        End While

        Return result
    End Function

    Private Shared Function CalculateClass(ByVal nb As UInt32) As IpClass
        Dim assertFirstBit As Boolean = ((nb And &H80000000&) = &H80000000&)
        Dim assertSecondBit As Boolean = ((nb And &H40000000&) = &H40000000&)

        If assertFirstBit And assertSecondBit Then
            Return IpClass.CLASS_C
        ElseIf assertFirstBit And Not assertSecondBit Then
            Return IpClass.CLASS_B
        ElseIf Not assertFirstBit Then
            Return IpClass.CLASS_A
        Else
            Return IpClass.OTHER
        End If
    End Function

#End Region

End Class
