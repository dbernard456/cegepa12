﻿
''' <summary>
''' Trame représentant un paquet
''' </summary>
''' <remarks>
''' David Bernard
''' David Frêve-Laverdière
''' </remarks>
Public Class Trame
    Public Const CRC_32 As UInt32 = &H4C11DB7

    Private _source As String
    Private _destination As String
    Private _message As String
    Private _fcs As UInt32

    Public Sub New(Source As String, Destination As String, MessageAEnvoyer As String)
        Me.Source = Source
        Me.Destination = Destination
        Me.Message = MessageAEnvoyer

        Me._fcs = &H0
    End Sub

    Public Sub New(trame As String)
        Dim characters As Char()
        Dim dest As String
        Dim source As String
        Dim msg As String
        Dim fcs As String
        Dim fcsRecu As UInt32

        characters = trame.ToCharArray

        source = ""
        For charCourant = 0 To 9
            source = source + characters(charCourant)

        Next

        dest = ""
        For charCourant = 10 To 19
            dest = dest + characters(charCourant)
        Next

        msg = ""
        For charCourant = 20 To 69
            msg = msg + characters(charCourant)
        Next

        fcs = ""
        For charCourant = 70 To 77
            fcs = fcs + characters(charCourant)
        Next

        fcsRecu = UInt32.Parse(fcs, Globalization.NumberStyles.AllowHexSpecifier)

        Me.Source = source
        Me.Destination = dest

        msg = msg.Trim()
        Me.Message = msg

        Me.CaculateFcs()

        If Me.Fcs <> fcsRecu Then
            Throw New Exception("ERREUR")
        End If
    End Sub

    Public Property Source As String
        Get
            Return Me._source
        End Get
        Set(value As String)
            Me._source = value
        End Set
    End Property

    Public Property Destination As String
        Get
            Return Me._destination
        End Get
        Set(value As String)
            Me._destination = value
        End Set
    End Property

    Public Property Message As String
        Get
            Return Me._message
        End Get
        Set(value As String)
            Me._message = value
            Me._message = Me._message.Trim
        End Set
    End Property

    Public ReadOnly Property Fcs As UInt32
        Get
            Return Me._fcs
        End Get
    End Property

    Public Sub resetFcs()
        Me._fcs = 0
    End Sub

    Public Sub InjecterErreur()
        Dim rand As New Random
        Dim errValue As UInt32
        Dim targetChar As UInt32
        Dim errLocation As Integer
        Dim chars As Char()
        Dim errorChar As Char
        Dim finalString As String

        If Me.Message.Length > 0 Then
            errLocation = rand.Next(0, Me.Message.Length)
            chars = Me.Message.ToCharArray
            targetChar = Asc(chars(errLocation))

            errValue = rand.Next(0, 8)
            targetChar = targetChar Xor errValue

            errValue = rand.Next(0, 8)
            targetChar = targetChar Xor errValue

            errorChar = Chr(targetChar)
            chars(errLocation) = errorChar

            finalString = ""

            For Each c In chars
                finalString = finalString + c
            Next

            Me.Message = finalString
        End If

    End Sub

    Public Sub CaculateFcs()
        Me.AddToFcs(Me.Message)
    End Sub

    Private Sub AddToFcs(content As String)
        Dim current As UInt32
        Dim reg As UInt32

        For Each character As Char In content
            ' Methode d'injection des nouveau
            current = Asc(character)
            current = current << 24
            Me._fcs = Me._fcs Xor current

            ' Division polymonial
            For bitCourant = 1 To 8
                reg = Me._fcs And &H80000000

                Me._fcs = Me._fcs << 1

                If reg > 0 Then
                    Me._fcs = Me._fcs Xor Trame.CRC_32
                End If
            Next
        Next
    End Sub

    Public Overrides Function toString() As String
        Dim hexFcs As String
        Dim outputString As String

        outputString = Me.Message

        hexFcs = Hex(Me.Fcs)
        hexFcs = hexFcs.PadLeft(8, "0")
        outputString = outputString + hexFcs


        outputString = outputString.PadLeft(78)

        Return outputString
    End Function

End Class
