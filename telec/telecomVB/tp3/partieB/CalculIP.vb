﻿Public Class CalculIP

    Private ip As AdresseIp
    Private Sub btnAnalyser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnalyser.Click

        Try
            Me.ip = New AdresseIp(Me.tbAdresseIp.Text, Integer.Parse(Me.tbSubnet.Text))

            Me.tbClasse.Text = Me.ip.Classe
            Me.tbMasqueCourt.Text = "/" & Me.ip.MaskNb
            Me.tbBroadcast.Text = Me.ip.BroadcastString
            Me.tbMasque.Text = Me.ip.MaskString
            Me.tbNbHost.Text = "" & Me.ip.NbHost

        Catch ex As Exception
            Me.tbClasse.Text = "ERROR"
            Me.tbMasqueCourt.Text = "ERROR"
            Me.tbBroadcast.Text = "ERROR"
            Me.tbMasque.Text = "ERROR"
            Me.tbNbHost.Text = "ERROR"
        End Try

    End Sub

    Private Sub btnQuitter_Click(sender As System.Object, e As System.EventArgs) Handles btnQuitter.Click
        Me.Close()
    End Sub

End Class