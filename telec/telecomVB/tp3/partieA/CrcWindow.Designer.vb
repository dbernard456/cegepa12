﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CrcWindow
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnTX = New System.Windows.Forms.Button()
        Me.BtnRX = New System.Windows.Forms.Button()
        Me.BtbErr = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TbSrc = New System.Windows.Forms.TextBox()
        Me.TbDst = New System.Windows.Forms.TextBox()
        Me.TbCharge = New System.Windows.Forms.TextBox()
        Me.TbFCS = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.BtnCalc = New System.Windows.Forms.Button()
        Me.ConsRX = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'BtnTX
        '
        Me.BtnTX.Location = New System.Drawing.Point(526, 244)
        Me.BtnTX.Name = "BtnTX"
        Me.BtnTX.Size = New System.Drawing.Size(75, 23)
        Me.BtnTX.TabIndex = 1
        Me.BtnTX.Text = "Transmettre"
        Me.BtnTX.UseVisualStyleBackColor = True
        '
        'BtnRX
        '
        Me.BtnRX.Location = New System.Drawing.Point(608, 244)
        Me.BtnRX.Name = "BtnRX"
        Me.BtnRX.Size = New System.Drawing.Size(75, 23)
        Me.BtnRX.TabIndex = 2
        Me.BtnRX.Text = "Recevoir"
        Me.BtnRX.UseVisualStyleBackColor = True
        '
        'BtbErr
        '
        Me.BtbErr.Location = New System.Drawing.Point(690, 244)
        Me.BtbErr.Name = "BtbErr"
        Me.BtbErr.Size = New System.Drawing.Size(75, 23)
        Me.BtbErr.TabIndex = 3
        Me.BtbErr.Text = "Erreur"
        Me.BtbErr.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(53, 111)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Adresse Source"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(34, 137)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Adresse Destination"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(70, 163)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Charge Utile"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(108, 189)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "FCS"
        '
        'TbSrc
        '
        Me.TbSrc.Location = New System.Drawing.Point(141, 108)
        Me.TbSrc.MaxLength = 10
        Me.TbSrc.Name = "TbSrc"
        Me.TbSrc.Size = New System.Drawing.Size(624, 20)
        Me.TbSrc.TabIndex = 8
        '
        'TbDst
        '
        Me.TbDst.Location = New System.Drawing.Point(141, 134)
        Me.TbDst.MaxLength = 10
        Me.TbDst.Name = "TbDst"
        Me.TbDst.Size = New System.Drawing.Size(624, 20)
        Me.TbDst.TabIndex = 9
        '
        'TbCharge
        '
        Me.TbCharge.Location = New System.Drawing.Point(141, 160)
        Me.TbCharge.MaxLength = 50
        Me.TbCharge.Name = "TbCharge"
        Me.TbCharge.Size = New System.Drawing.Size(624, 20)
        Me.TbCharge.TabIndex = 10
        '
        'TbFCS
        '
        Me.TbFCS.Location = New System.Drawing.Point(141, 186)
        Me.TbFCS.Name = "TbFCS"
        Me.TbFCS.ReadOnly = True
        Me.TbFCS.Size = New System.Drawing.Size(624, 20)
        Me.TbFCS.TabIndex = 11
        Me.TbFCS.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(96, 35)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(196, 31)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "CRC 32 Testor"
        '
        'BtnCalc
        '
        Me.BtnCalc.Location = New System.Drawing.Point(445, 243)
        Me.BtnCalc.Name = "BtnCalc"
        Me.BtnCalc.Size = New System.Drawing.Size(75, 23)
        Me.BtnCalc.TabIndex = 13
        Me.BtnCalc.Text = "Calc CRC32"
        Me.BtnCalc.UseVisualStyleBackColor = True
        '
        'ConsRX
        '
        Me.ConsRX.Location = New System.Drawing.Point(37, 316)
        Me.ConsRX.Name = "ConsRX"
        Me.ConsRX.ReadOnly = True
        Me.ConsRX.Size = New System.Drawing.Size(728, 231)
        Me.ConsRX.TabIndex = 14
        Me.ConsRX.TabStop = False
        Me.ConsRX.Text = ""
        '
        'CrcWindow
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(821, 592)
        Me.Controls.Add(Me.ConsRX)
        Me.Controls.Add(Me.BtnCalc)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TbFCS)
        Me.Controls.Add(Me.TbCharge)
        Me.Controls.Add(Me.TbDst)
        Me.Controls.Add(Me.TbSrc)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtbErr)
        Me.Controls.Add(Me.BtnRX)
        Me.Controls.Add(Me.BtnTX)
        Me.Name = "CrcWindow"
        Me.Text = "CrcWindow"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BtnTX As System.Windows.Forms.Button
    Friend WithEvents BtnRX As System.Windows.Forms.Button
    Friend WithEvents BtbErr As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TbSrc As System.Windows.Forms.TextBox
    Friend WithEvents TbDst As System.Windows.Forms.TextBox
    Friend WithEvents TbCharge As System.Windows.Forms.TextBox
    Friend WithEvents TbFCS As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BtnCalc As System.Windows.Forms.Button
    Friend WithEvents ConsRX As System.Windows.Forms.RichTextBox
End Class
