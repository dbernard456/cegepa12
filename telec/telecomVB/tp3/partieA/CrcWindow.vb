﻿
''' <summary>
''' Interface graphique du testeur crc.
''' </summary>
''' <remarks>
''' David Bernard
''' David Frêve-Laverdière
''' </remarks>
Public Class CrcWindow
    Private currentTrame As Trame

    Public Sub New()

        ' Cet appel est requis par le concepteur.
        InitializeComponent()

        Me.currentTrame = New Trame("", "", "")
        Me.Label5.Text = "Testeur de crc 0x" + Hex(Trame.CRC_32).PadLeft(8, "0")
        Me.Text = "David Bernard & David Frève-Laverdière"

        Me.CalcFCS()

    End Sub

    Private Sub CrcWindow_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load



    End Sub

    Private Sub BtnTX_Click(sender As System.Object, e As System.EventArgs) Handles BtnTX.Click
        Dim buffer As String
        Dim trameRecu As Trame

        Me.currentTrame.Message = Me.TbCharge.Text
        buffer = Me.currentTrame.toString

        Try
            trameRecu = New Trame(buffer)
            Me.ConsRX.Text = Me.ConsRX.Text + trameRecu.Message + vbNewLine
        Catch ex As Exception
            MsgBox("Erreur de transmission!!! difference de CRC 32")
        End Try
    End Sub

    Private Sub BtnRX_Click(sender As System.Object, e As System.EventArgs) Handles BtnRX.Click

    End Sub

    Private Sub BtnCalc_Click(sender As System.Object, e As System.EventArgs) Handles BtnCalc.Click
        Me.CalcFCS()
    End Sub

    Private Sub BtbErr_Click(sender As System.Object, e As System.EventArgs) Handles BtbErr.Click
        Me.currentTrame.Message = Me.TbCharge.Text
        Me.currentTrame.InjecterErreur()
        Me.TbCharge.Text = Me.currentTrame.Message
    End Sub

    Private Sub CalcFCS()
        Dim fcs As UInt32
        Dim outputStr As String

        Me.currentTrame.resetFcs()
        Me.currentTrame.Message = Me.TbCharge.Text
        Me.currentTrame.CaculateFcs()

        fcs = Me.currentTrame.Fcs

        outputStr = ""
        outputStr = outputStr + "0x" + Hex(fcs).PadLeft(8, "0")

        outputStr = outputStr + " || "

        outputStr = outputStr + fcs.ToString


        Me.TbFCS.Text = outputStr
    End Sub


End Class