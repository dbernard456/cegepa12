﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Globalization;
using System.IO;

namespace VideoThumbnailer
{
    public delegate void ImageDelegate(Image image);
    public delegate void CaputureDelegate(Boolean SameAsPrevious);

    public class AutoImageBuilder
    {
        private static int DPI = 96;

        private static readonly int TEXT_SIZE = 10;
        private static readonly PixelFormat PIXEL_FORMAT = PixelFormats.Pbgra32;
        private static readonly CultureInfo CULTURE = CultureInfo.GetCultureInfo("en-us");
        private static readonly FlowDirection FLOW_DIRECTION = FlowDirection.LeftToRight;
        private static readonly Typeface TYPEFACE = new Typeface("Verdana");
        private static readonly SolidColorBrush COLOR = Brushes.Red;

        public event Action ReadyToCapture;
        public event CaputureDelegate CaptureAvailaible;

        private RenderTargetBitmap bmp;
        private Queue<TimeSpan> positionsToThumbnail;
        private MediaPlayer mediaPlayer;
        private Boolean ready;
        private Image mainImage;

        private int currentX;
        private int currentY;
        private int currentI;
        private int currentJ;
        private int width;
        private int height;
        private int totalWidth;
        private int totalHeight;
        private int columns;
        private int lines;

        private int count;
        private int totalThumbnails;
        private Double scale;
        private uint[] framePixels;
        private uint[] previousFramePixels;

        public AutoImageBuilder(Image mainImage, int columns, int lines, Double imageScale)
        {
            this.ready = false;

            this.currentI = 0;
            this.currentJ = 0;

            this.columns = columns;
            this.lines = lines;

            this.scale = imageScale;

            this.count = 0;
            this.totalThumbnails = this.columns * this.lines;

            this.currentX = 0;
            this.currentY = 0;

            this.positionsToThumbnail = new Queue<TimeSpan>();

            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.ScrubbingEnabled = true;
            this.mediaPlayer.Pause();
            this.mediaPlayer.MediaOpened += new EventHandler(mediaPlayer_MediaOpened);
            this.mediaPlayer.Changed += new EventHandler(mediaPlayer_Changed);

            this.mainImage = mainImage;
        }

        private void mediaPlayer_Changed(object sender, EventArgs e)
        {
            this.mediaPlayer.Pause();
            if (CaptureAvailaible != null)
            {
                this.CaptureAvailaible(this.PixelBuffelsAreEquals);
            }
        }

        private void mediaPlayer_MediaOpened(object sender, EventArgs e)
        {
            this.ManageMediaPlayerOpened();
        }

        public Boolean Ready
        {
            get
            {
                return this.ready;
            }
        }

        public Boolean PixelBuffelsAreEquals
        {
            get
            {
                return this.framePixels.SequenceEqual(this.previousFramePixels);
            }
        }

        public Image MainImage
        {
            get
            {
                return this.mainImage;
            }
        }
        public double TotalMilliseconds
        {
            get
            {
                return this.mediaPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
            }
        }

        public String Source
        {
            set
            {
                this.mediaPlayer.Open(new Uri(value));
            }
        }

        public int PixelCount
        {
            get
            {
                return this.mediaPlayer.NaturalVideoWidth * this.mediaPlayer.NaturalVideoHeight;
            }
        }

        public Boolean HasNext
        {
            get
            {
                return this.positionsToThumbnail.Count > 0;
            }
        }

        public int Count
        {
            get
            {
                return this.count;
            }
        }

        public int TotalThumbnails
        {
            get
            {
                return this.totalThumbnails;
            }
        }

        private void InitBuffers()
        {
            int pixelCount;

            pixelCount = this.PixelCount;

            this.framePixels = new uint[pixelCount];
            this.previousFramePixels = new uint[pixelCount];
        }

        private void SeekToNextThumbnailPosition()
        {
            // If more frames remain to capture...
            if (this.HasNext)
            {
                // Seek to next position and start watchdog timer
                this.mediaPlayer.Position = this.positionsToThumbnail.Dequeue();
                //   this.watchdogTimer.Start();
            }
            else
            {
                // Done; close media file and stop processing
                this.mediaPlayer.Close();

                this.framePixels = null;
                this.previousFramePixels = null;

                this.ready = false;
            }
        }


        public void Capture()
        {
            this.RenderBitmapAndCapturePixels(this.framePixels);
        }

        private void SwapPixelBuffer()
        {
            uint[] tempPixels;

            // Swap the pixel buffers (moves current to previous and avoids allocating a new buffer for current)
            tempPixels = this.framePixels;
            this.framePixels = this.previousFramePixels;
            this.previousFramePixels = tempPixels;
        }

        private void ManageMediaPlayerOpened()
        {
            TimeSpan timespan;
            double totalMilliseconds;
            double delay;

            totalMilliseconds = this.TotalMilliseconds;

            this.InitBuffers();

            // Enqueue a position for each frame (at the center of each of the N segments)
            for (int i = 0; i < this.TotalThumbnails; i++)
            {
                delay = (((2 * i) + 1) * totalMilliseconds) / (2 * this.TotalThumbnails);

                timespan = TimeSpan.FromMilliseconds(delay);
                this.positionsToThumbnail.Enqueue(timespan);

                Console.WriteLine("time = " + FormatTimeSpan(timespan));
            }

            this.width = (int)(this.mediaPlayer.NaturalVideoWidth * this.scale);
            this.height = (int)(this.mediaPlayer.NaturalVideoHeight * this.scale);

            this.totalWidth = this.width * this.columns;
            this.totalHeight = this.height * this.lines;

            this.bmp = new RenderTargetBitmap(this.totalWidth, this.totalHeight, DPI, DPI, PIXEL_FORMAT);

            this.RenderBitmapAndCapturePixels(this.previousFramePixels);

            this.ready = true;

            if (this.ReadyToCapture != null)
            {
                this.ReadyToCapture();
            }
        }

        public Boolean Next()
        {
            Boolean ok = this.Ready && this.HasNext;

            if (ok)
            {
                this.SeekToNextThumbnailPosition();
            }

            return ok;
        }

        private void RenderBitmapAndCapturePixels(uint[] pixels)
        {
            DrawingVisual drawingVisual;
            RenderTargetBitmap renderTargetBitmap;
            Rect rect;
            String textToDraw;
            FormattedText text;
            int naturalWidth;
            int naturalHeight;

            naturalWidth = this.mediaPlayer.NaturalVideoWidth;
            naturalHeight = this.mediaPlayer.NaturalVideoHeight;

            // Render the current frame into a bitmap
            drawingVisual = new DrawingVisual();
            renderTargetBitmap = new RenderTargetBitmap(naturalWidth, naturalHeight, DPI, DPI, PixelFormats.Default);

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                rect = new Rect(this.currentX, this.currentY, this.width, this.height);
                drawingContext.DrawVideo(this.mediaPlayer, rect);

                // Dessiner le temps sur l'image
                textToDraw = AutoImageBuilder.FormatTimeSpan(this.mediaPlayer.Position);
                text = new FormattedText(textToDraw, CULTURE, FLOW_DIRECTION, TYPEFACE, TEXT_SIZE, COLOR);
                drawingContext.DrawText(text, new Point(this.currentX, this.currentY));
            }


            this.currentI++;

            if (this.currentI >= this.columns)
            {
                this.currentJ++;
                this.currentI = 0;
            }

            // Recommencer au début si le nombre de lignes a été dépasser.
            if (this.currentJ >= this.lines)
            {
                this.currentJ = 0;
            }

            this.currentX = this.currentI * this.width;
            this.currentY = this.currentJ * this.height;


            renderTargetBitmap.Render(drawingVisual);

            // Copy the pixels to the specified location
            //renderTargetBitmap.CopyPixels(pixels, naturalWidth * 4, 0);

            this.bmp.Render(drawingVisual);
            this.mainImage.Source = this.bmp;

            this.SwapPixelBuffer();

            this.count++;
        }

        public static String FormatTimeSpan(TimeSpan time)
        {
            int hours;

            String output;
            String HoursString;
            String minuteString;
            String secondString;


            // Heures
            hours = time.Hours;
            if (hours > 0)
            {
                HoursString = hours + ":";
            }
            else
            {
                HoursString = "";
            }

            // Minutes
            minuteString = "" + time.Minutes;
            if (minuteString.Length < 2)
            {
                minuteString = "0" + minuteString;
            }

            minuteString += ":";

            // Seconds
            secondString = "" + time.Seconds;
            if (secondString.Length < 2)
            {
                secondString = "0" + secondString;
            }


            output = "";
            output += HoursString;
            output += minuteString;
            output += secondString;


            return output;
        }

        public static void SaveImage(Image image, String desiredPath)
        {
            FileStream stream = null;
            JpegBitmapEncoder encoder = null;
            BitmapSource source = null;
            BitmapFrame bmpFrame = null;

            using (stream = new FileStream(desiredPath, FileMode.Create))
            {

                try
                {
                    encoder = new JpegBitmapEncoder();

                    source = (BitmapSource)image.Source;
                    bmpFrame = BitmapFrame.Create(source);
                    encoder.Frames.Add(bmpFrame);


                    encoder.Save(stream);
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Flush();
                        stream.Close();
                    }
                }
            }
        }
    }
}
