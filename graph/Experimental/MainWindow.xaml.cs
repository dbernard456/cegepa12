﻿using System;
using System.Reflection;
using System.Resources;
using System.Windows;
using MediaReader.Time;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace Experimental
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly int DPI = 96;

        private static readonly PixelFormat PIXEL_FORMAT = PixelFormats.Pbgra32;

        MediaPlayer player = new MediaPlayer();
        VideoDrawing aVideoDrawing = new VideoDrawing();
        public MainWindow()
        {
            InitializeComponent();

            // Create a VideoDrawing. 
            //      


            player.MediaOpened += new EventHandler(player_MediaOpened);
            player.Open(new Uri(@"C:\temp\media\PC46.wmv"));




        }

        void player_MediaOpened(object sender, EventArgs e)
        {
            aVideoDrawing.Changed += new EventHandler(player_Changed);
            aVideoDrawing.Rect = new Rect(0, 0, 100, 100);

            aVideoDrawing.Player = player;
        }



        void player_Changed(object sender, EventArgs e)
        {
            DrawingVisual drawingVisual;
            RenderTargetBitmap renderTargetBitmap;

            renderTargetBitmap = new RenderTargetBitmap((int)image1.Width, (int)image1.Height, DPI, DPI, PIXEL_FORMAT);

            // Render the current frame into a bitmap
            drawingVisual = new DrawingVisual();

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawDrawing(aVideoDrawing);
            }

            renderTargetBitmap.Render(drawingVisual);
            this.image1.Source = renderTargetBitmap;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            player.Play();
            // this.TestTimeDivision();
        }

        public void TestRes()
        {
            String output = "";

            ResourceManager rm = new ResourceManager("Strings", Assembly.GetExecutingAssembly());

            // Retrieve the value of the string resource named "welcome".
            // The resource manager will retrieve the value of the  
            // localized resource using the caller's current culture setting.
            output = Experimental.Properties.Resources.String1;

            Console.WriteLine(output);
        }

        public void TestTimeDivision()
        {
            TimeSpan ts = new TimeSpan(3, 0, 0);
            TimeSpan[] spans = TimeUtils.DivideTimeSpans(ts, 6);

            foreach (TimeSpan s in spans)
            {
                Console.WriteLine("" + s.Hours + " " + s.Minutes + " " + s.Seconds);
            }

            if (spans[spans.Length - 1] > ts)
            {
                Console.WriteLine("OVERFLOW");
            }

        }

        private void image1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }
    }
}
