﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Microsoft.Win32;

namespace VideoThumbnailer
{
    public partial class Window1 : Window
    {
        private readonly DependencyProperty FileNameProperty;
        private readonly DependencyProperty ProcessingProperty;


        // Member variables
        private AutoImageBuilder imageBuilder;


        public Window1()
        {
            FrameworkPropertyMetadata frameworkPropertyMetadata;

            // Initialize window and hook up to events of interest
            this.InitializeComponent();

            FileNameProperty = DependencyProperty.Register("FileName", typeof(string), typeof(Window1));

            frameworkPropertyMetadata = new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits);
            ProcessingProperty = DependencyProperty.RegisterAttached("Processing", typeof(bool), typeof(Window1), frameworkPropertyMetadata);

            this.imageBuilder = new AutoImageBuilder(this.MainImage, 5, 5, .3);

            this.imageBuilder.ReadyToCapture += new Action(imageBuilder_ReadyToCapture);
            this.imageBuilder.CaptureAvailaible += new CaputureDelegate(imageBuilder_CaptureAvailible);

            this.AddHandler(Button.ClickEvent, new RoutedEventHandler(HandleOpenFileButtonClick));

        }

        void imageBuilder_ReadyToCapture()
        {
            this.SetProcessing(this, true);
            this.imageBuilder.Next();
        }

        void imageBuilder_CaptureAvailible(bool SameAsPrevious)
        {
            if (this.imageBuilder.Count < this.imageBuilder.TotalThumbnails)
            {
                this.imageBuilder.Capture();
            }

            Console.WriteLine("Count = " + this.imageBuilder.Count);

            if (!this.imageBuilder.Next())
            {
                this.SetProcessing(this, false);
            }
        }

        // Dependency property indicates the file name of the current media
        public string FileName
        {
            get
            {
                return (string)this.GetValue(FileNameProperty);
            }
            set
            {
                this.SetValue(FileNameProperty, value);
            }
        }

        private void HandleOpenFileButtonClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog;
            String fileName;
            bool? result;

            // Display open file dialog
            openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            openFileDialog.Filter = "Video Files (*.wmv;*.dvr-ms;*.mpeg;*.mpg;*.avi)|*.wmv;*.dvr-ms;*.mpeg;*.mpg;*.avi|All Files (*)|*";

            // Show dialog
            result = openFileDialog.ShowDialog();

            // If a file was chosen...
            if (result.HasValue && result.Value)
            {
                // Reset state
                fileName = openFileDialog.FileName;



                this.FileName = fileName;

                // Open media file
                this.imageBuilder.Source = fileName;
            }
        }




        // Inheritable attached dependency property indicates whether processing is going on
        public bool GetProcessing(DependencyObject obj)
        {
            return (bool)obj.GetValue(this.ProcessingProperty);
        }

        public void SetProcessing(DependencyObject obj, bool value)
        {
            obj.SetValue(this.ProcessingProperty, value);
        }


    }
}
