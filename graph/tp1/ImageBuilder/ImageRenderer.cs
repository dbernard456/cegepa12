﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MediaReader.FileSystem;
using MediaReader.Time;

namespace MediaReader.ImageBuilder
{
    /// <summary>
    /// Classe qui s'occupe des détails de la construction de vignettes.
    /// 
    /// David Bernard
    /// </summary>
    class ImageRenderer
    {
        public event Action CaptureAvailaible;

        private static readonly int HEADER_HEIGHT = 100;

        private static readonly int TEXT_SIZE = 10;
        private static readonly int HEADER_TEXT_SIZE = 20;

        private readonly RenderTargetBitmap bitmap;
        private readonly Image mainImage;
        private readonly MediaPlayer mediaPlayer;
        private readonly MediaFile mediaFile;
        private readonly String backgroundImage;
        private readonly double scale;
        private readonly int naturalVideoWidth;
        private readonly int naturalVideoHeight;
        private readonly int imageWidth;
        private readonly int imageHeight;
        private readonly int frameWidth;
        private readonly int frameHeight;
        private readonly int columns;
        private readonly int lines;
        private readonly int totalWidth;
        private readonly int totalHeight;
        private readonly int totalThumbnails;
        private readonly int marginX;
        private readonly int marginY;

        private int currentX;
        private int currentY;
        private int currentI;
        private int currentJ;

        public ImageRenderer(MediaPlayer mediaPlayer, MediaFile mediaFile, String backgroundImage, int columns, int lines, int marginX, int marginY, double scale)
        {
            Boolean valid;

            valid = true;
            valid = valid && ImageBuilderUtils.ValidateScale(scale);
            valid = valid && ImageBuilderUtils.ValidateColumn(columns);
            valid = valid && ImageBuilderUtils.ValidateLine(lines);
            valid = valid && ImageBuilderUtils.ValidateMargin(marginX);
            valid = valid && ImageBuilderUtils.ValidateMargin(marginY);

            if (!valid)
            {
                throw new Exception();
            }

            this.mediaPlayer = mediaPlayer;
            this.mediaFile = mediaFile;
            this.backgroundImage = backgroundImage;
            this.columns = columns;
            this.lines = lines;
            this.scale = scale;
            this.marginX = marginX;
            this.marginY = marginY;

            this.naturalVideoWidth = this.mediaPlayer.NaturalVideoWidth;
            this.naturalVideoHeight = this.mediaPlayer.NaturalVideoHeight;
            this.imageWidth = (int)(this.naturalVideoWidth * this.scale);
            this.imageHeight = (int)(this.naturalVideoHeight * this.scale);
            this.frameWidth = this.imageWidth + this.marginX * 2;
            this.frameHeight = this.imageHeight + this.marginY * 2;
            this.totalWidth = this.frameWidth * this.columns;
            this.totalHeight = this.frameHeight * this.lines + HEADER_HEIGHT;
            this.totalThumbnails = this.columns * this.lines;

            try
            {
                this.bitmap = new RenderTargetBitmap(this.totalWidth, this.totalHeight, ImageBuilderUtils.DPI, ImageBuilderUtils.DPI, ImageBuilderUtils.PIXEL_FORMAT);
            }
            catch (Exception)
            {
                throw new ImageMemoryOverflowException();
            }


            this.mainImage = new Image();

            this.currentI = 0;
            this.currentJ = 0;
            this.currentX = 0;
            this.currentY = 0;

            this.PrepareHeader();

            if (this.backgroundImage != null)
            {
                this.PrepareBackground();
            }

            this.mediaPlayer.Changed += new EventHandler(mediaPlayer_Changed);
        }

        private void mediaPlayer_Changed(object sender, EventArgs e)
        {
            if (this.CaptureAvailaible != null)
            {
                this.CaptureAvailaible();
            }
        }

        public TimeSpan Position
        {
            set
            {
                this.mediaPlayer.Position = value;
            }
        }

        public int TotalThumbnails
        {
            get
            {
                return this.totalThumbnails;
            }
        }

        public Image MainImage
        {
            get
            {
                return this.mainImage;
            }
        }

        public MediaFile Source
        {
            get
            {
                return this.mediaFile;
            }
        }


        public int NaturalVideoWidth
        {
            get
            {
                return this.naturalVideoWidth;
            }
        }

        public int NaturalVideoHeight
        {
            get
            {
                return this.naturalVideoHeight;
            }
        }

        public int ImageWidth
        {
            get
            {
                return this.imageWidth;
            }
        }

        public int ImageHeight
        {
            get
            {
                return this.imageHeight;
            }
        }

        public int FrameWidth
        {
            get
            {
                return this.frameWidth;
            }
        }

        public int FrameHeight
        {
            get
            {
                return this.frameHeight;
            }
        }

 

        public void Reset()
        {
            this.currentI = 0;
            this.currentJ = 0;
            this.currentX = 0;
            this.currentY = 0;
        }

        private void PrepareHeader()
        {
            DrawingVisual drawingVisual;
            String textToDraw;
            FormattedText text;
            int pos = 0;

            drawingVisual = new DrawingVisual();

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawRectangle(Brushes.Black, new Pen(), new Rect(0, 0, this.totalWidth, HEADER_HEIGHT));

                // Dessiner le temps sur l'image
                textToDraw = "Name = " + this.mediaFile.SimpleName;
                text = ImageBuilderUtils.GenerateFormattedText(textToDraw, Brushes.Red, HEADER_TEXT_SIZE);
                drawingContext.DrawText(text, new Point(0, pos));
                pos += HEADER_TEXT_SIZE;

                textToDraw = "Path = " + this.mediaFile.LocalPath;
                text = ImageBuilderUtils.GenerateFormattedText(textToDraw, Brushes.Red, HEADER_TEXT_SIZE);
                drawingContext.DrawText(text, new Point(0, pos));
                pos += HEADER_TEXT_SIZE;

                textToDraw = "Duration = " + this.mediaFile.TotalDurationString;
                text = ImageBuilderUtils.GenerateFormattedText(textToDraw, Brushes.Red, HEADER_TEXT_SIZE);
                drawingContext.DrawText(text, new Point(0, pos));
                pos += HEADER_TEXT_SIZE;

                textToDraw = "Size = " + Math.Round(this.mediaFile.Megabytes, 2) + " mb";
                text = ImageBuilderUtils.GenerateFormattedText(textToDraw, Brushes.Red, HEADER_TEXT_SIZE);
                drawingContext.DrawText(text, new Point(0, pos));
                pos += HEADER_TEXT_SIZE;
            }

            this.bitmap.Render(drawingVisual);
            this.mainImage.Source = this.bitmap;
        }

        private void PrepareBackground()
        {
            DrawingVisual drawingVisual;
            RenderTargetBitmap renderTargetBitmap;
            ImageSource source;

            drawingVisual = new DrawingVisual();

            renderTargetBitmap = new RenderTargetBitmap(this.NaturalVideoWidth, this.NaturalVideoHeight, ImageBuilderUtils.DPI, ImageBuilderUtils.DPI, ImageBuilderUtils.PIXEL_FORMAT);

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                source = ImageBuilderUtils.RetreiveJpegSource(this.backgroundImage);
                drawingContext.DrawImage(source, new Rect(0, HEADER_HEIGHT, this.totalWidth, this.totalHeight - HEADER_HEIGHT));
            }

            renderTargetBitmap.Render(drawingVisual);
            this.bitmap.Render(drawingVisual);
            this.mainImage.Source = this.bitmap;
        }


        public void Capture(int sleepTime)
        {
            Thread.Sleep(sleepTime);

            this.Capture();
        }

        public void Capture()
        {
            DrawingVisual drawingVisual;
            Rect rect;
            String textToDraw;
            FormattedText text;

            // Render the current frame into a bitmap
            drawingVisual = new DrawingVisual();

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                rect = new Rect(this.currentX + this.marginX, this.currentY + this.marginY + HEADER_HEIGHT, this.imageWidth, this.imageHeight);
                drawingContext.DrawVideo(this.mediaPlayer, rect);

                // Dessiner le temps sur l'image
                textToDraw = TimeUtils.FormatTimeSpan(this.mediaPlayer.Position);
                text = ImageBuilderUtils.GenerateFormattedText(textToDraw, Brushes.Red, TEXT_SIZE);
                drawingContext.DrawText(text, new Point(this.currentX + this.marginX, this.currentY + this.marginY + HEADER_HEIGHT));
            }

            this.currentI++;

            if (this.currentI >= this.columns)
            {
                this.currentJ++;
                this.currentI = 0;
            }

            // Recommencer au début si le nombre de lignes a été dépasser.
            if (this.currentJ >= this.lines)
            {
                this.currentJ = 0;
            }

            this.currentX = this.currentI * this.FrameWidth;
            this.currentY = this.currentJ * this.FrameHeight;

            this.bitmap.Render(drawingVisual);
            this.mainImage.Source = this.bitmap;
        }
    }
}
