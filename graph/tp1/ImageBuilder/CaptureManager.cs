﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MediaReader.FileSystem;
using System.Diagnostics;

namespace MediaReader.ImageBuilder
{
    /// <summary>
    /// Classe qui gère les construction d'image.
    /// 
    /// David Bernard
    /// </summary>
    public class CaptureManager
    {
        public event Action PreparationDone;
        public event Action CaptureStateChanged;

        private static readonly int SLEEP_TIME = 300;

        private readonly Queue<TimeSpan> positionsToThumbnail;
        private readonly MediaPlayer mediaPlayer;
        private readonly Double scale;

        private readonly int columns;
        private readonly int lines;
        private readonly int marginX;
        private readonly int marginY;

        private MediaFile mediaFile;
        private VideoDrawing videoDrawing = new VideoDrawing();
        private ImageRenderer manualImageBuilder;
        private RenderTargetBitmap videoBitmap;
        private Boolean ready;
        private Boolean closed;
        private Boolean captureState;
        private Image videoImage;
        private String backgroundImage;


        public CaptureManager(int columns, int lines, int marginX, int marginY, Double imageScale)
        {
            this.backgroundImage = null;
            this.ready = false;
            this.closed = false;
            this.captureState = false;

            this.columns = columns;
            this.lines = lines;
            this.marginX = marginX;
            this.marginY = marginY;
            this.scale = imageScale;

            this.positionsToThumbnail = new Queue<TimeSpan>();

            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.ScrubbingEnabled = true;
            this.mediaPlayer.Pause();
            this.mediaPlayer.IsMuted = true;
            this.mediaPlayer.MediaOpened += new EventHandler(mediaPlayer_MediaOpened);
        }

        void manualImageBuilder_CaptureAvailaible()
        {
            this.manualImageBuilder.CaptureAvailaible -= new Action(manualImageBuilder_CaptureAvailaible);

            this.Capture();

            if (this.HasNext)
            {
                this.SeekNextCapture();
            }
            else
            {
                this.setCaputureState(false);
            }
        }

        private void mediaPlayer_MediaOpened(object sender, EventArgs e)
        {
            try
            {
                this.manualImageBuilder = new ImageRenderer(this.mediaPlayer, this.mediaFile, this.backgroundImage, this.columns, this.lines, this.marginX, this.marginY, this.scale);
               
                this.videoImage = new Image();

                this.videoDrawing = new VideoDrawing();
                this.videoBitmap = new RenderTargetBitmap(this.manualImageBuilder.ImageWidth, this.manualImageBuilder.ImageHeight, ImageBuilderUtils.DPI, ImageBuilderUtils.DPI, ImageBuilderUtils.PIXEL_FORMAT);
                this.videoDrawing.Rect = new Rect(0, 0, this.manualImageBuilder.ImageWidth, this.manualImageBuilder.ImageHeight);
                this.videoDrawing.Player = this.mediaPlayer;
                this.videoDrawing.Changed += new EventHandler(videoDrawing_Changed);

                this.RenderVideo();

                this.ready = true;

            }
            catch (ImageMemoryOverflowException)
            {
                this.ready = false;
                this.closed = true;
            }


            if (this.PreparationDone != null)
            {
                this.PreparationDone();
            }
        }

        private void videoDrawing_Changed(object sender, EventArgs e)
        {
            this.RenderVideo();


            // Libéré la mémoire utilisé afin d'éviter de défoncé la mémoire disponible.
            GC.Collect();
        }

        private void RenderVideo()
        {
            DrawingVisual drawingVisual;




            // Render the current frame into a bitmap
            drawingVisual = new DrawingVisual();

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawDrawing(this.videoDrawing);
            }

            videoBitmap.Render(drawingVisual);
            this.videoImage.Source = videoBitmap;
        }

        public String BackgroundImage
        {
            get
            {
                return this.backgroundImage;
            }
            set
            {
                this.backgroundImage = value;
            }
        }

        public Boolean CaptureState
        {
            get
            {
                return this.captureState;
            }
        }

        public Boolean Ready
        {
            get
            {
                return this.ready;
            }
        }

        public Image MainImage
        {
            get
            {
                return this.manualImageBuilder.MainImage;
            }
        }

        public Image VideoImage
        {
            get
            {
                return this.videoImage;
            }
        }

        public double TotalMilliseconds
        {
            get
            {
                return this.mediaPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
            }
        }

        public void Open(MediaFile mediaFile)
        {
            if (this.closed)
            {
                throw new Exception("Image builder is closed");
            }

            this.mediaFile = mediaFile;
            this.mediaPlayer.Open(this.mediaFile.Uri);

        }

        public MediaFile Source
        {
            get
            {
                return this.mediaFile;
            }
        }

        public Boolean HasNext
        {
            get
            {
                return (this.positionsToThumbnail.Count > 0);
            }
        }

        public int TotalThumbnails
        {
            get
            {
                return this.manualImageBuilder.TotalThumbnails;
            }
        }

        private void SeekNextCapture()
        {
            if (this.HasNext)
            {
                this.manualImageBuilder.CaptureAvailaible += new Action(manualImageBuilder_CaptureAvailaible);
                this.manualImageBuilder.Position = this.positionsToThumbnail.Dequeue();
            }
            else
            {
                this.setCaputureState(false);
            }
        }

        private void Capture()
        {
            lock (this.manualImageBuilder)
            {
                this.manualImageBuilder.Capture();

                // Libéré la mémoire utilisé afin d'éviter de défoncé la mémoire disponible.
                GC.Collect();
            }
        }

        private void DequeueAll()
        {
            this.DequeueAll(true);
        }

        private void DequeueAll(Boolean forceSleepTime)
        {
            if (this.closed)
            {
                throw new Exception("image builder is closed");
            }

            if (!this.ready)
            {
                throw new Exception("image builder is not ready");
            }

            this.setCaputureState(true);

            if (this.HasNext)
            {
                this.manualImageBuilder.Position = this.positionsToThumbnail.Dequeue();

                if (forceSleepTime)
                {
                    this.Capture();
                }
                else
                {
                    this.manualImageBuilder.Capture(SLEEP_TIME);
                }

                this.SeekNextCapture();
            }
            else
            {
                this.setCaputureState(false);
            }
        }

        public void TakeSnapshot()
        {
            this.positionsToThumbnail.Enqueue(this.mediaPlayer.Position);

            this.DequeueAll(false);
        }

        public void AutoCapture()
        {
            double totalMilliseconds;
            double delay;
            TimeSpan timespan;

            this.Reset();
            this.mediaPlayer.Stop();

            totalMilliseconds = this.TotalMilliseconds;

            // Enqueue a position for each frame (at the center of each of the N segments)
            for (int i = 0; i < this.manualImageBuilder.TotalThumbnails; i++)
            {
                delay = (((2 * i) + 1) * totalMilliseconds) / (2 * this.manualImageBuilder.TotalThumbnails);

                timespan = TimeSpan.FromMilliseconds(delay);
                this.positionsToThumbnail.Enqueue(timespan);
            }

            this.DequeueAll();
        }

        public void Reset()
        {
            this.positionsToThumbnail.Clear();

            this.manualImageBuilder.Reset();

            this.mediaPlayer.Stop();
        }

        public void Play()
        {
            this.mediaPlayer.Play();
        }

        private void setCaputureState(Boolean captureState)
        {
            this.captureState = captureState;

            if (this.CaptureStateChanged != null)
            {
                this.CaptureStateChanged();
            }
        }

        public void SaveCurrentImage()
        {
            ImageBuilderUtils.SaveImage(this.manualImageBuilder.MainImage, this.mediaPlayer.Source.LocalPath + ".jpg");
        }
    }
}
