﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MediaReader.ImageBuilder
{
    /// <summary>
    /// Classe utilitaire pour image builder.
    /// 
    /// David Bernard
    /// </summary>
    class ImageBuilderUtils
    {
        private static readonly CultureInfo CULTURE = CultureInfo.GetCultureInfo("en-us");
        private static readonly FlowDirection FLOW_DIRECTION = FlowDirection.LeftToRight;
        private static readonly Typeface TYPEFACE = new Typeface("Verdana");

        public static readonly int DPI = 96;
        public static readonly PixelFormat PIXEL_FORMAT = PixelFormats.Pbgra32;
        public static readonly int MAX_MARGIN = 50;
        public static readonly int MAX_LINES_COUNT = 10;
        public static readonly int MAX_COLUMNS_COUNT = 10;

        private ImageBuilderUtils() { }

        public static FormattedText GenerateFormattedText(String text, SolidColorBrush color, int size)
        {
            return new FormattedText(text, CULTURE, FLOW_DIRECTION, TYPEFACE, size, color);
        }

        public static Boolean ValidateMargin(int margin)
        {
            return margin >= 0 && margin <= MAX_MARGIN;
        }

        public static Boolean ValidateScale(double scale)
        {
            return scale > 0 && scale <= 1;
        }

        public static Boolean ValidateLine(int lines)
        {
            return lines > 0 && lines <= MAX_LINES_COUNT;
        }

        public static Boolean ValidateColumn(int columns)
        {
            return columns > 0 && columns <= MAX_COLUMNS_COUNT;
        }

        public static double ParseScaleString(String doubleString)
        {
            return Double.Parse(doubleString, CULTURE);
        }

        public static String ConvertDoubleToString(double d)
        {
            return d.ToString(CULTURE);
        }

        public static ImageSource RetreiveJpegSource(String path)
        {
            Stream imageStreamSource = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            JpegBitmapDecoder decoder = new JpegBitmapDecoder(imageStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            BitmapSource bitmapSource = decoder.Frames[0];

            return bitmapSource;
        }

        public static void SaveImage(Image image, String desiredPath)
        {
            FileStream stream = null;
            JpegBitmapEncoder encoder = null;
            BitmapSource source = null;
            BitmapFrame bmpFrame = null;

            try
            {
                stream = new FileStream(desiredPath, FileMode.Create);
                encoder = new JpegBitmapEncoder();

                source = (BitmapSource)image.Source;
                bmpFrame = BitmapFrame.Create(source);
                encoder.Frames.Add(bmpFrame);

                encoder.Save(stream);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                }
            }

        }
    }
}
