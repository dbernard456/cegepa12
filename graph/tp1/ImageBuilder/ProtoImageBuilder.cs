﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MediaReader.Time;

namespace MediaReader.ImageBuilder
{
    public class ProtoImageBuilder
    {
        private static readonly int DPI = 96;
        private static readonly int TEXT_SIZE = 10;
        private static readonly PixelFormat PIXEL_FORMAT = PixelFormats.Pbgra32;
        private static readonly CultureInfo CULTURE = CultureInfo.GetCultureInfo("en-us");
        private static readonly FlowDirection FLOW_DIRECTION = FlowDirection.LeftToRight;
        private static readonly Typeface TYPEFACE = new Typeface("Verdana");
        private static readonly SolidColorBrush COLOR = Brushes.Red;

        private DrawingVisual drawingVisual;
        private MediaElement mediaElement;
        private RenderTargetBitmap bmp;
        private Image mainImage;

        private int currentX;
        private int currentY;
        private int currentI;
        private int currentJ;
        private int width;
        private int height;
        private int totalWidth;
        private int totalHeight;
        private int columns;
        private int lines;
        private int nbImages;
        private Boolean closed;
        private Double scale;

        public ProtoImageBuilder(MediaElement element, int parameterLines, int parameterColumns, Double imageScale)
        {
            this.mediaElement = element;
            this.nbImages = 0;
            this.closed = false;

            this.currentI = 0;
            this.currentJ = 0;

            this.columns = parameterColumns;
            this.lines = parameterLines;

            this.scale = imageScale;
            this.width = (int)(element.ActualWidth * imageScale);
            this.height = (int)(element.ActualHeight * imageScale);

            this.totalWidth = this.width * parameterColumns;
            this.totalHeight = this.height * parameterLines;

            this.currentX = 0;
            this.currentY = 0;

            this.mainImage = new Image();
            this.drawingVisual = new DrawingVisual();

            this.bmp = new RenderTargetBitmap(this.totalWidth, this.totalHeight, DPI, DPI, PIXEL_FORMAT);
        }

        public void RenderMedia()
        {
            FormattedText text;
            DrawingContext drawingContext;
            RenderTargetBitmap bmpCapture;
            Rect rect;
            String textToDraw;

            bmpCapture = new RenderTargetBitmap(this.MediaElementWidth, this.MediaElementHeight, DPI, DPI, PIXEL_FORMAT);
            bmpCapture.Render(this.mediaElement);

            drawingContext = this.drawingVisual.RenderOpen();
            rect = new Rect(this.currentX, this.currentY, this.width, this.height);
            drawingContext.DrawImage(bmpCapture, rect);

            // Dessiner le temps sur l'image
            textToDraw = TimeUtils.FormatTimeSpan(this.mediaElement.Position);
            text = new FormattedText(textToDraw, CULTURE, FLOW_DIRECTION, TYPEFACE, TEXT_SIZE, COLOR);
            drawingContext.DrawText(text, new Point(this.currentX, this.currentY));

            drawingContext.Close();

            this.currentI++;

            if (this.currentI >= this.columns)
            {
                this.currentJ++;
                this.currentI = 0;
            }

            // Recommencer au début si le nombre de lignes a été dépasser.
            if (this.currentJ >= this.lines)
            {
                this.currentJ = 0;
            }

            this.currentX = this.currentI * this.width;
            this.currentY = this.currentJ * this.height;

            this.bmp.Render(this.drawingVisual);
            this.mainImage.Source = this.bmp;

            this.nbImages++;
        }

        public void launchAutoCapture(int imageCount)
        {
            TimeSpan[] frames = null;
            this.mediaElement.LoadedBehavior = MediaState.Play;
            Thread.Sleep(200);
            this.mediaElement.LoadedBehavior = MediaState.Pause;

            this.mediaElement.LoadedBehavior = MediaState.Play;
            Thread.Sleep(200);
            this.mediaElement.LoadedBehavior = MediaState.Pause;

            frames = TimeUtils.DivideTimeSpans(TimeUtils.AnalyseTotalDuration(this.mediaElement.Source), imageCount);

            foreach (TimeSpan frame in frames)
            {
                this.mediaElement.Position = frame;

                this.mediaElement.LoadedBehavior = MediaState.Play;
                Thread.Sleep(200);
                this.mediaElement.LoadedBehavior = MediaState.Pause;

                this.RenderMedia();
            }
            this.mediaElement.LoadedBehavior = MediaState.Stop;
            this.mediaElement.ScrubbingEnabled = false;
        }

        #region Properties
        private int MediaElementWidth
        {
            get
            {
                return (int)mediaElement.ActualWidth;
            }
        }

        private int MediaElementHeight
        {
            get
            {
                return (int)mediaElement.ActualHeight;
            }
        }

        public Boolean Closed
        {
            get
            {
                return this.closed;
            }
        }

        public Image MainImage
        {
            get
            {
                return this.mainImage;
            }
        }
        #endregion

        public void CloseImage(String desiredPath)
        {
            FileStream stream = null;
            JpegBitmapEncoder encoder = null;
            BitmapSource source;
            BitmapFrame bmpFrame;

            if (!this.Closed && this.nbImages > 0)
            {
                try
                {
                    encoder = new JpegBitmapEncoder();

                    source = (BitmapSource)this.mainImage.Source;
                    bmpFrame = BitmapFrame.Create(source);
                    encoder.Frames.Add(bmpFrame);

                    stream = new FileStream(desiredPath, FileMode.Create);
                    encoder.Save(stream);
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Flush();
                        stream.Close();
                    }

                    this.closed = true;
                }
            }
        }
    }
}
