﻿using System;

namespace MediaReader.Time
{
    /// <summary>
    /// Classe d'utilitaires de temps.
    /// 
    /// David Bernard
    /// </summary>
    public class TimeUtils
    {
        private TimeUtils() { }

        public static String FormatTimeSpan(TimeSpan time)
        {
            int hours;

            String output;
            String HoursString;
            String minuteString;
            String secondString;


            // Heures
            hours = time.Hours;
            if (hours > 0)
            {
                HoursString = hours + ":";
            }
            else
            {
                HoursString = "";
            }

            // Minutes
            minuteString = "" + time.Minutes;
            if (minuteString.Length < 2)
            {
                minuteString = "0" + minuteString;
            }

            minuteString += ":";

            // Seconds
            secondString = "" + time.Seconds;
            if (secondString.Length < 2)
            {
                secondString = "0" + secondString;
            }


            output = "";
            output += HoursString;
            output += minuteString;
            output += secondString;


            return output;
        }

        public static TimeSpan CalculateTimeSpan(Double duration)
        {
            TimeSpan durationSpan;
            int durationInt = 0;
            int seconds = 0;
            int milliseconds = 0;

            durationInt = (int)(duration * 1000);

            // Diviser par 60 secondes
            seconds = durationInt / 1000;

            durationInt -= seconds * 1000;

            milliseconds = durationInt;

            durationSpan = new TimeSpan(0, 0, 0, seconds, milliseconds);

            return durationSpan;
        }

        public static TimeSpan[] DivideTimeSpans(TimeSpan timeSpan, int dividor)
        {
            TimeSpan[] output = null;
            TimeSpan tempTimeSpan;
            int interval;
            int currentSecond = 0;


            if (dividor > 0 && timeSpan != null)
            {
                output = new TimeSpan[dividor];
                interval = (int)(timeSpan.TotalSeconds / dividor);
                currentSecond = interval / 2;

                for (int i = 0; i < output.Length; i++)
                {
                    tempTimeSpan = new TimeSpan(0, 0, currentSecond);
                    currentSecond += interval;
                    output[i] = tempTimeSpan;
                }
            }

            return output;
        }

        public static TimeSpan AnalyseTotalDuration(Uri uri)
        {
            WMPLib.WindowsMediaPlayer wmp = new WMPLib.WindowsMediaPlayer();
            WMPLib.IWMPMedia mediaInfo = wmp.newMedia(uri.LocalPath);
            return TimeUtils.CalculateTimeSpan(mediaInfo.duration);
        }
    }
}
