﻿using System;
using System.Windows.Threading;
using MediaReader.Core;

namespace MediaReader.Time
{
    /// <summary>
    /// Classe qui gère le temps.
    /// 
    /// David BernardS
    /// </summary>
    public class TimeManager : IMediaInterpret
    {
        public event EventHandler TickEvent;
        private DispatcherTimer timer;
        private Boolean running;
        private IMediaEventDispatcher mediaEventDispatcher;

        public TimeManager()
        {
            this.timer = new DispatcherTimer();
            this.timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            this.timer.Tick += new EventHandler(timer_Tick);
        }

        public Boolean Running
        {
            get
            {
                return this.running;
            }
        }

        private void Stop()
        {
            if (this.Running)
            {
                this.timer.Stop();
                this.running = false;
            }
        }

        private void Start()
        {
            if (!this.Running)
            {
                this.timer.Start();
                this.running = true;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (this.TickEvent != null)
            {
                this.TickEvent(this, null);
            }
        }

        public IMediaEventDispatcher LocalEventDispatcher
        {
            set
            {
                if (value != null)
                {
                    this.mediaEventDispatcher = value;
                    this.mediaEventDispatcher.CurrentMediaStateChanged += new EventHandler(mediaEventDispatcher_CurrentMediaStateChanged);
                }
            }
            get
            {
                return this.mediaEventDispatcher;
            }
        }

        void mediaEventDispatcher_CurrentMediaStateChanged(Object sender, EventArgs e)
        {
            if (this.running && this.mediaEventDispatcher.Stopped)
            {
                this.Stop();
            }
            else if (!this.running && this.mediaEventDispatcher.Playing)
            {
                this.Start();
            }
        }


    }
}
