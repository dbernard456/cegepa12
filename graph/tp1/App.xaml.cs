﻿using System;
using System.Windows;

namespace MediaReader
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static String commandLineParams;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            if (e.Args.Length > 0)
            {
                App.commandLineParams = e.Args[0];
            }
            else
            {
                App.commandLineParams = null;
            }
        }

        public static String CommandLineParam
        {
            get
            {
                return App.commandLineParams;
            }
        }
    }
}