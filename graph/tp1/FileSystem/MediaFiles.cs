﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;

namespace MediaReader.FileSystem
{
    public class MediaCollection : ObservableCollection<MediaFile>
    {
        private String localPath;
        private Boolean dirty;
        private Boolean newlyCreated;

        public MediaCollection()
        {
            this.dirty = false;
            this.newlyCreated = true;
            this.LocalPath = null;
        }

        public String LocalPath
        {
            get
            {
                return this.localPath;
            }

            set
            {
                this.localPath = value;
            }
        }

        public Boolean HasPath
        {
            get
            {
                return this.localPath != null;
            }
        }

        public MediaFile GetFileAt(int index)
        {
            return this.ElementAt(index);
        }

        public new void RemoveAt(int index)
        {
            this.dirty = true;
            base.RemoveAt(index);
        }

        public new void Add(MediaFile mediaFile)
        {
            if (!this.MediaFileExists(mediaFile))
            {
                this.dirty = true;
                base.Add(mediaFile);
            }
        }

        public Boolean MediaFileExists(MediaFile mediaFile)
        {
            Boolean exists = false;

            if (mediaFile != null)
            {
                foreach (MediaFile file in this)
                {
                    if (file.SimpleName.Equals(mediaFile.SimpleName))
                    {
                        exists = true;
                    }
                }
            }

            return exists;
        }

        public new void Clear()
        {
            this.dirty = true;
            base.Clear();
        }


        public MediaFile[] MediaFilesList()
        {
            int i = 0;
            MediaFile[] array = new MediaFile[this.Count];

            foreach (MediaFile file in this)
            {
                array[i] = file;
                i++;
            }

            return array;
        }

        public Boolean Dirty
        {
            get
            {
                return this.dirty;
            }
        }

        public Boolean NewlyCreated
        {
            get
            {
                return this.newlyCreated;
            }
        }

        public void Reset()
        {
            this.Clear();
            this.dirty = true;
            this.newlyCreated = true;
            this.localPath = null;
        }

        public void WriteMediaStore()
        {
            XmlWriter xmlWriter = null;

            if (this.HasPath)
            {
                using (xmlWriter = XmlWriter.Create(this.LocalPath))
                {
                    try
                    {

                        xmlWriter.WriteStartDocument();
                        xmlWriter.WriteStartElement("mediaStore");

                        foreach (MediaFile file in this)
                        {
                            xmlWriter.WriteStartElement("media");
                            xmlWriter.WriteAttributeString("path", file.LocalPath);
                            xmlWriter.WriteEndElement();
                        }

                        xmlWriter.WriteEndElement();
                        xmlWriter.WriteEndDocument();
                    }
                    finally
                    {
                        if (xmlWriter != null)
                        {
                            xmlWriter.Flush();
                            xmlWriter.Close();
                        }
                    }
                }

                this.newlyCreated = false;
                this.dirty = false;
            }
        }


        public void ReadMediaStore()
        {
            MediaFile media = null;
            String name = null;
            String path = null;
            Uri uri;
            XmlReader reader = null;

            if (File.Exists(this.LocalPath))
            {
                try
                {
                    this.Clear();
                    reader = XmlReader.Create(this.LocalPath);

                    while (reader.Read())
                    {
                        name = reader.Name;

                        if (name.Equals("media"))
                        {
                            path = reader.GetAttribute("path");
                            uri = new Uri(path);
                            media = new MediaFile(uri);
                            this.Add(media);
                        }
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }

                this.newlyCreated = false;
                this.dirty = false;
            }
        }

        public static String CreateStoreFile()
        {
            return FileUtils.QuickSaveFileDialog("Xml File|*.xml", "Save Playlist");
        }
    }
}
