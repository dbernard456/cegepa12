﻿using System;
using System.Globalization;
using System.IO;
using MediaReader.Time;

namespace MediaReader.FileSystem
{
    public enum MediaType { Video, Audio, Image, Unmanaged };

    /// <summary>
    /// Classe qui représente un fichier média.
    /// </summary>
    public class MediaFile
    {
        public const String VideoTypes = "*.wmv;*.mp4;*.avi";
        public const String AudioTypes = "*.wav;*.mp3";
        public const String ImageTypes = "*.jpg;*.bmb";

        private Uri uri;
        private MediaType mediaType;
        private TimeSpan duration;
        private String fileName;
        private String extension;
        private long byteLength;

        public MediaFile(Uri uri)
        {
            WMPLib.WindowsMediaPlayer wmp;
            WMPLib.IWMPMedia mediaInfo;
            FileInfo fileInfo;
            String ext;

            this.uri = uri;

            wmp = new WMPLib.WindowsMediaPlayer();
            mediaInfo = wmp.newMedia(this.LocalPath);
            fileInfo = new FileInfo(this.LocalPath);

            ext = Path.GetExtension(this.uri.LocalPath);

            this.extension = Path.GetExtension(ext);
            this.mediaType = MediaFile.CheckTypeByExtension(ext);
            this.duration = TimeUtils.AnalyseTotalDuration(uri);
            this.fileName = Path.GetFileName(this.uri.LocalPath);
            this.byteLength = fileInfo.Length;
        }

        #region Properties
        public long ByteLength
        {
            get
            {
                return this.byteLength;
            }
        }

        public String ParentFolder
        {
            get
            {
                FileInfo info = new FileInfo(this.LocalPath);

                return info.Directory.FullName;
            }
        }

        public Double Megabytes
        {
            get
            {
                return ((Double)this.byteLength / (1024 * 1024));
            }
        }

        public String Extension
        {
            get
            {
                return this.extension;
            }
        }

        public String SimpleName
        {
            get
            {
                return this.fileName;
            }
        }

        public String LocalPath
        {
            get
            {
                return uri.LocalPath;
            }
        }

        public MediaType MediaType
        {
            get
            {
                return this.mediaType;
            }
        }

        public TimeSpan Duration
        {
            get
            {
                return this.duration;
            }
        }


        public String TotalDurationString
        {
            get
            {
                String output = "-";

                if (this.HasTimeSpan)
                {
                    output = TimeUtils.FormatTimeSpan((TimeSpan)this.Duration);
                }

                return output;
            }
        }

        public Boolean HasTimeSpan
        {
            get
            {
                return this.duration.TotalMilliseconds > 0;
            }
        }

        public Uri Uri
        {
            get
            {
                return this.uri;
            }
        }

        public Double TotalMilliseconds
        {
            get
            {
                return this.duration.TotalMilliseconds;
            }
        }

        public String MediaTypeString
        {
            get
            {
                return "" + this.mediaType;
            }
        }
        #endregion

        #region Utils
        public static MediaType CheckTypeByExtension(String extension)
        {
            MediaType output = output = MediaType.Unmanaged;

            if (extension != null)
            {
                extension = extension.ToLower(CultureInfo.CurrentCulture);
                extension = extension.Replace(".", "");

                if (MediaFile.VideoTypes.Contains(extension))
                {
                    output = MediaType.Video;
                }
                else if (MediaFile.AudioTypes.Contains(extension))
                {
                    output = MediaType.Audio;
                }
                else if (MediaFile.ImageTypes.Contains(extension))
                {
                    output = MediaType.Image;
                }
            }

            return output;
        }

        public static Uri OpenMediaFileDialog()
        {
            String filter = "";

            filter += "Media files|" + MediaFile.VideoTypes + ";" + MediaFile.AudioTypes + ";" + MediaFile.ImageTypes;
            filter += "|";

            filter += "Video files|" + MediaFile.VideoTypes;
            filter += "|";

            filter += "Audio files|" + MediaFile.AudioTypes;
            filter += "|";

            filter += "Image files|" + MediaFile.ImageTypes;
            filter += "|";

            filter += "All files (*.*)|*.*";

            return FileUtils.OpenFileDialog(filter);
        }
        #endregion
    }
}
