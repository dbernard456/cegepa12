﻿using System;
using System.Windows;
using Microsoft.Win32;

namespace MediaReader.FileSystem
{
    class FileUtils
    {
        private FileUtils() { }

        public static Uri OpenFileDialog(String extensions)
        {
            string filename;
            Uri uri = null;
            Nullable<bool> result;
            OpenFileDialog dialog;

            dialog = new OpenFileDialog();
            dialog.FileName = "Document";
            dialog.Filter = extensions;

            result = dialog.ShowDialog();

            if (result == true)
            {
                filename = dialog.FileName;

                uri = new Uri(filename);
            }

            return uri;
        }

        public static String QuickSaveFileDialog(String filter, String dialogTitle)
        {
            String output = null;

            SaveFileDialog dialog = new SaveFileDialog();


            dialog.Filter = filter;
            dialog.Title = dialogTitle;

            dialog.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (!String.IsNullOrEmpty(dialog.FileName))
            {
                output = dialog.FileName;
            }

            return output;
        }

        public static Uri[] manageFileDrop(DragEventArgs e)
        {
            string[] filesName;
            string fileName;
            Uri[] fileUris;
            Uri fileUri;

            filesName = (string[])e.Data.GetData(DataFormats.FileDrop);

            fileUris = new Uri[filesName.Length];
            for (int i = 0; i < filesName.Length; i++)
            {
                fileName = filesName[i];
                fileUri = new Uri(fileName);
                fileUris[i] = fileUri;
            }

            return fileUris;
        }

        public static Uri OpenXMLFileDialog()
        {
            return FileUtils.OpenFileDialog("Xml documents|*.xml|All files (*.*)|*.*");
        }



    }

}
