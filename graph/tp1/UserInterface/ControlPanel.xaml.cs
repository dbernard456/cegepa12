﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using MediaReader.Core;

namespace MediaReader
{
    /// <summary>
    /// Logique d'interaction pour ControlPanel.xaml
    /// </summary>
    public partial class ControlPanel : UserControl, IMediaInterpret
    {
        private Boolean durationSliderMoving;
        private IMediaEventDispatcher mediaEventDispatcher;

        public ControlPanel()
        {
            InitializeComponent();

            this.durationSliderMoving = false;

            this.stopButton.IsEnabled = false;
            this.playButton.IsEnabled = false;
            this.volumeSlider.IsEnabled = false;
            this.speedSlider.IsEnabled = false;
            this.durationSlider.IsEnabled = false;

            this.playButton.Click += new RoutedEventHandler(this.playButton_Click);
            this.stopButton.Click += new RoutedEventHandler(this.stopButton_Click);
        }

        #region properties
        public Boolean DurationSliderMoving
        {
            get
            {
                return this.durationSliderMoving;
            }
        }

        public void RefreshControlEnabled()
        {
            this.durationSlider.IsEnabled = this.mediaEventDispatcher.HasTimeSpan;
            this.durationSlider.Maximum = this.mediaEventDispatcher.TotalDurationMilliseconds;
            this.volumeSlider.IsEnabled = this.mediaEventDispatcher.VolumeControlShouldBeEnabled;
            this.speedSlider.IsEnabled = this.mediaEventDispatcher.SpeedControlShouldBeEnabled;
            this.totalTime.Content = this.mediaEventDispatcher.TotalDurationString;
            this.fileLabel.Content = this.mediaEventDispatcher.MediaFileLabel;
            this.volumeSlider.Value = this.mediaEventDispatcher.DesiredVolume;

            this.playButton.Content = this.mediaEventDispatcher.PlayButtonText;
            this.playButton.IsEnabled = this.mediaEventDispatcher.PlayButtonShouldBeEnabled;
            this.stopButton.IsEnabled = this.mediaEventDispatcher.StopButtonShouldBeEnabled;
        }

        private Double DesiredSpeedRatio
        {
            get
            {
                Double speed = this.speedSlider.Value;
                Double tempSpeedRatio = 1.0;

                if (speed >= 4.0)
                {
                    tempSpeedRatio = 4.0;
                }
                else if (speed >= 3.0)
                {
                    tempSpeedRatio = 2.0;
                }
                else if (speed >= 2.0)
                {
                    tempSpeedRatio = 1;
                }
                else if (speed >= 1.0)
                {
                    tempSpeedRatio = 0.5;
                }
                else if (speed >= 0.0)
                {
                    tempSpeedRatio = 0.25;
                }

                return tempSpeedRatio;
            }
        }

        private Double CalculateSpeedSliderValueForRealSpeed()
        {
            Double speed = this.speedSlider.Value;
            Double tempSpeedRatio = this.mediaEventDispatcher.DesiredSpeed;

            if (tempSpeedRatio <= 0.25)
            {
                speed = 0.0;
            }
            else if (tempSpeedRatio <= 0.5)
            {
                speed = 1.0;
            }
            else if (tempSpeedRatio <= 1)
            {
                speed = 2.0;
            }
            else if (tempSpeedRatio <= 2.0)
            {
                speed = 3.0;
            }
            else if (tempSpeedRatio <= 4.0)
            {
                speed = 4.0;
            }

            return speed;

        }
        #endregion

        #region handlers
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void volumeSlider_DragDelta(object sender, DragDeltaEventArgs e)
        {
            this.mediaEventDispatcher.DeclareNewDesiredVolume(this.volumeSlider.Value);
            this.VolumeIndicator.Content = this.mediaEventDispatcher.VolumeIndicatorText;
        }

        private void speedSlider_DragDelta(object sender, DragDeltaEventArgs e)
        {
            this.mediaEventDispatcher.DeclareNewDesiredSpeed(this.DesiredSpeedRatio);
            this.SpeedIndicator.Content = this.mediaEventDispatcher.SpeedIndicatorText;
        }

        private void durationSlider_DragDelta(object sender, DragDeltaEventArgs e)
        {
            this.mediaEventDispatcher.DurationSliderDelta((int)this.durationSlider.Value);
        }

        private void durationSlider_DragStarted(object sender, DragStartedEventArgs e)
        {
            this.durationSliderMoving = true;
            this.mediaEventDispatcher.DurationSliderStart();
        }

        private void durationSlider_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            this.mediaEventDispatcher.DurationSliderStop();
            this.durationSliderMoving = false;
        }
        #endregion

        #region Buttons Handlers

        private void playButton_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.ManagePlayButton();
        }


        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.ManageStopButton();
        }
        #endregion

        #region MediaEventDispatcher
        public IMediaEventDispatcher LocalEventDispatcher
        {
            set
            {
                if (value != null)
                {
                    this.mediaEventDispatcher = value;

                    // Rafraichir les indicateurs des vitesse et de volume.
                    this.volumeSlider.Value = this.mediaEventDispatcher.DesiredVolume;
                    this.VolumeIndicator.Content = this.mediaEventDispatcher.VolumeIndicatorText;
                    this.speedSlider.Value = this.CalculateSpeedSliderValueForRealSpeed();
                    this.SpeedIndicator.Content = this.mediaEventDispatcher.SpeedIndicatorText;



                    this.mediaEventDispatcher.CurrentMediaFileChanged += new EventHandler(mediaEventDispatcher_CurrentMediaFileChanged);
                    this.mediaEventDispatcher.CurrentMediaStateChanged += new EventHandler(mediaEventDispatcher_CurrentMediaStateChanged);
                    this.mediaEventDispatcher.Tick += new EventHandler(mediaEventDispatcher_Tick);
                }
            }
            get
            {
                return this.mediaEventDispatcher;
            }
        }

        private void mediaEventDispatcher_CurrentMediaStateChanged(Object sender, EventArgs e)
        {
            this.RefreshControlEnabled();
        }

        private void mediaEventDispatcher_Tick(Object sender, EventArgs e)
        {
            this.currentTime.Content = this.mediaEventDispatcher.CurrentDurationString;
            this.durationSlider.Value = this.mediaEventDispatcher.DurationSliderValue;
        }

        private void mediaEventDispatcher_CurrentMediaFileChanged(Object sender, EventArgs e)
        {
            this.RefreshControlEnabled();
        }

        #endregion

        private void durationSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }
}
