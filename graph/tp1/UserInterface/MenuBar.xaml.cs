﻿using System;
using System.Windows;
using System.Windows.Controls;
using MediaReader.Core;
using MediaReader.FileSystem;

namespace MediaReader
{
    /// <summary>
    /// Logique d'interaction pour MenuBar.xaml
    /// </summary>
    public partial class MenuBar : UserControl, IMediaInterpret
    {
        private IMediaEventDispatcher mediaEventDispatcher;

        public MenuBar()
        {
            InitializeComponent();

            // File Menu
            this.MenuFichierOuvrir.Click += new RoutedEventHandler(MenuFichierOuvrir_Click);
            this.MenuFichierQuitter.Click += new RoutedEventHandler(MenuFichierQuitter_Click);

            // Media Menu
            this.MenuMediaPlay.Click += new RoutedEventHandler(MenuMediaPlay_Click);
            this.MenuMediaStop.Click += new RoutedEventHandler(MenuMediaStop_Click);

            // Playlist Menu
            this.MenuPlayListDeleteAll.Click += new RoutedEventHandler(MenuPlayListDeleteAll_Click);
            this.MenuPlayListShow.Click += new RoutedEventHandler(MenuPlayListShow_Click);
            this.MenuPlayListSave.Click += new RoutedEventHandler(MenuPlayListSave_Click);
            this.MenuPlayListLoad.Click += new RoutedEventHandler(MenuPlayListLoad_Click);
            this.MenuPlayListSaveAs.Click += new RoutedEventHandler(MenuPlayListSaveAs_Click);
            this.MenuPlayListNew.Click += new RoutedEventHandler(MenuPlayListNew_Click);

            // Capture Menu
            this.MenuCaptureManager.Click += new RoutedEventHandler(MenuCaptureManager_Click);
        }

        void MenuCaptureManager_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.RequestLaunchCaptureManager();
        }

        void MenuPlayListNew_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.CreateNewPlaylistRequest();
        }

        void MenuPlayListSaveAs_Click(object sender, RoutedEventArgs e)
        {
            String uri = MediaCollection.CreateStoreFile();

            if (uri != null)
            {
                this.mediaEventDispatcher.ChangePlaylistPathRequest(uri);
                this.mediaEventDispatcher.SavePlaylistRequest();
            }
        }

        void MenuPlayListLoad_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = FileUtils.OpenXMLFileDialog();

            this.mediaEventDispatcher.ManagePlaylistLoadRequest(uri);
        }

        void MenuPlayListSave_Click(object sender, RoutedEventArgs e)
        {
            String path;

            if (this.mediaEventDispatcher.PlaylistIsNew)
            {
                path = MediaCollection.CreateStoreFile();

                if (path != null)
                {
                    this.mediaEventDispatcher.ChangePlaylistPathRequest(path);
                    this.mediaEventDispatcher.SavePlaylistRequest();
                }
            }
            else
            {
                this.mediaEventDispatcher.SavePlaylistRequest();
            }
        }

        void MenuPlayListShow_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.RequestShowPlaylistWindow();
        }

        void MenuPlayListDeleteAll_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.PurgePlaylistRequest();
        }

        void MenuMediaStop_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.ManageStopButton();
        }

        void MenuMediaPlay_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.ManagePlayButton();
        }

        void MenuFichierQuitter_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.RequestApplicationClose();
        }

        void MenuFichierOuvrir_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = MediaFile.OpenMediaFileDialog();

            this.mediaEventDispatcher.ManageMediaUriRequest(uri);
        }

        #region MediaEventDispatcher
        public IMediaEventDispatcher LocalEventDispatcher
        {
            set
            {
                if (value != null)
                {
                    this.mediaEventDispatcher = value;
                    this.mediaEventDispatcher.CurrentMediaFileChanged += new EventHandler(mediaEventDispatcher_CurrentMediaFileChanged);
                    this.mediaEventDispatcher.CurrentMediaStateChanged += new EventHandler(mediaEventDispatcher_CurrentMediaStateChanged);

                    this.MenuMediaPlay.IsEnabled = this.mediaEventDispatcher.PlayButtonShouldBeEnabled;
                    this.MenuMediaStop.IsEnabled = this.mediaEventDispatcher.StopButtonShouldBeEnabled;
                }
            }
            get
            {
                return this.mediaEventDispatcher;
            }
        }

        void mediaEventDispatcher_CurrentMediaStateChanged(Object sender, EventArgs e)
        {
            this.MenuMediaPlay.Header = this.mediaEventDispatcher.PlayButtonText;
            this.MenuMediaPlay.IsEnabled = this.mediaEventDispatcher.PlayButtonShouldBeEnabled;
            this.MenuMediaStop.IsEnabled = this.mediaEventDispatcher.StopButtonShouldBeEnabled;
        }

        void mediaEventDispatcher_CurrentMediaFileChanged(Object sender, EventArgs e)
        {
            this.MenuMediaPlay.IsEnabled = this.mediaEventDispatcher.PlayButtonShouldBeEnabled;
            this.MenuMediaStop.IsEnabled = this.mediaEventDispatcher.StopButtonShouldBeEnabled;

           this.MenuCaptureManager.IsEnabled = this.mediaEventDispatcher.HasMediaFile && this.mediaEventDispatcher.CurrentMediaFile.HasTimeSpan;
        }
        #endregion
    }
}
