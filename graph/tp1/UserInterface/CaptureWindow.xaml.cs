﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using MediaReader.FileSystem;
using MediaReader.ImageBuilder;
using MediaReader.Core;

namespace MediaReader
{
    /// <summary>
    /// Interaction logic for CaptureWindow.xaml
    /// </summary>
    public partial class CaptureWindow : Window
    {
        private readonly MediaFile source;
        private readonly MediaConfig configs;
        private String currentBackgroundPath;

        private CaptureManager autoImageBuilder;

        public CaptureWindow(MediaConfig configs, MediaFile file)
        {
            InitializeComponent();

            this.source = file;
            this.configs = configs;

            this.Closed += new EventHandler(CaptureWindow_Closed);
            this.ExitButton.Click += new RoutedEventHandler(ExitButton_Click);
            this.CaptureButton.Click += new RoutedEventHandler(CaptureButton_Click);
            this.PrepareButton.Click += new RoutedEventHandler(PrepareButton_Click);
            this.SaveImageButton.Click += new RoutedEventHandler(SaveImageButton_Click);
            this.PlayButton.Click += new RoutedEventHandler(PlayButton_Click);
            this.AutoCaptureButton.Click += new RoutedEventHandler(AutoCaptureButton_Click);
            this.BackgroundButton.Click += new RoutedEventHandler(BackgroundButton_Click);

            this.DesiredLines = this.configs.LineCount;
            this.DesiredColumns = this.configs.ColumnCount;
            this.currentBackgroundPath = this.configs.Background;
            this.DesiredScale = this.configs.Scale;
            this.DesiredMarginX = this.configs.MarginX;
            this.DesiredMarginY = this.configs.MarginY;

            this.MediaTitleLabel.Content = file.SimpleName;

            this.Reset();

            this.labelScale.Content = "Scale ]0, 1] (1.0 = 100%)";
            this.labelColumns.Content = "Columns ]0, " + ImageBuilderUtils.MAX_COLUMNS_COUNT + "]";
            this.labelLine.Content = "Lines ]0, " + ImageBuilderUtils.MAX_LINES_COUNT + "]";
            this.labelMarginX.Content = "Margin (x) [0, " + ImageBuilderUtils.MAX_MARGIN + "]";
            this.labelMarginY.Content = "Margin (y) [0, " + ImageBuilderUtils.MAX_MARGIN + "]";
        }

        private void RefreshButtons()
        {
            Boolean captureOk;

            captureOk = this.autoImageBuilder != null && this.autoImageBuilder.Ready && !this.autoImageBuilder.CaptureState && this.ValidateParameters();

            this.AutoCaptureButton.IsEnabled = captureOk;
            this.CaptureButton.IsEnabled = captureOk;
            this.SaveImageButton.IsEnabled = captureOk;
            this.PlayButton.IsEnabled = captureOk;
            

            this.PrepareButton.IsEnabled = (this.autoImageBuilder != null && !this.autoImageBuilder.CaptureState) || this.autoImageBuilder == null;
            this.ExitButton.IsEnabled = (this.autoImageBuilder != null && !this.autoImageBuilder.CaptureState) || this.autoImageBuilder == null;
            this.BackgroundButton.IsEnabled = (this.autoImageBuilder != null && !this.autoImageBuilder.CaptureState) || this.autoImageBuilder == null;

        }

        private void CaptureWindow_Closed(object sender, EventArgs e)
        {
            if (this.ValidateParameters())
            {
                this.configs.ColumnCount = this.DesiredColumns;
                this.configs.LineCount = this.DesiredLines;
                this.configs.Scale = this.DesiredScale;
                this.configs.Background = this.currentBackgroundPath;
                this.configs.MarginX = this.DesiredMarginX;
                this.configs.MarginY = this.DesiredMarginY;
            }
        }

        void BackgroundButton_Click(object sender, RoutedEventArgs e)
        {
            Uri uri;

            uri = FileUtils.OpenFileDialog("Jpeg File|*.jpg");

            if (uri != null)
            {
                this.currentBackgroundPath = uri.LocalPath;
                this.Reset();
            }
        }

        void AutoCaptureButton_Click(object sender, RoutedEventArgs e)
        {
            this.autoImageBuilder.AutoCapture();
        }

        void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            this.autoImageBuilder.Play();
        }

        void CaptureButton_Click(object sender, RoutedEventArgs e)
        {
            this.autoImageBuilder.TakeSnapshot();
        }

        void ExitSaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ValidateParameters())
            {
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Invalid parameters");
            }
        }

        void SaveImageButton_Click(object sender, RoutedEventArgs e)
        {
            this.autoImageBuilder.SaveCurrentImage();
        }

        void PrepareButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ValidateParameters())
            {
                this.Reset();
            }
            else
            {
                MessageBox.Show("Invalid parameters");
            }
        }

        private void Reset()
        {
            if (this.autoImageBuilder != null)
            {
                // Enlever les anciens ecouteurs.
                this.autoImageBuilder.PreparationDone -= new Action(autoImageBuilder_PreparationDone);
                this.autoImageBuilder.CaptureStateChanged -= new Action(autoImageBuilder_CaptureBegin);
            }

            this.autoImageBuilder = new CaptureManager(this.DesiredLines, this.DesiredColumns, this.DesiredMarginX, this.DesiredMarginY, this.DesiredScale);
            this.autoImageBuilder.BackgroundImage = this.currentBackgroundPath;
            this.autoImageBuilder.Open(this.source);

            this.autoImageBuilder.PreparationDone += new Action(autoImageBuilder_PreparationDone);
            this.autoImageBuilder.CaptureStateChanged += new Action(autoImageBuilder_CaptureBegin);


            this.ImageHolder.Children.Clear();

            GC.Collect();
        }

        void autoImageBuilder_CaptureBegin()
        {
            this.RefreshButtons();
        }

        private void autoImageBuilder_PreparationDone()
        {
            Image image;

            if (this.autoImageBuilder.Ready)
            {
                image = this.autoImageBuilder.MainImage;
                this.ImageHolder.Children.Clear();
                this.ImageHolder.Children.Add(image);
                image.Stretch = System.Windows.Media.Stretch.Uniform;
                image.VerticalAlignment = System.Windows.VerticalAlignment.Top;

                image = this.autoImageBuilder.VideoImage;
                this.VideoHolder.Children.Clear();
                this.VideoHolder.Children.Add(image);
                image.Stretch = System.Windows.Media.Stretch.Uniform;
                image.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            }
            else
            {
                MessageBox.Show("Error while initializing image");
            }


            this.RefreshButtons();
        }



        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private Boolean ValidateParameters()
        {
            Boolean valid;

            valid = true;

            valid = valid && ImageBuilderUtils.ValidateLine(this.DesiredLines);
            valid = valid && ImageBuilderUtils.ValidateColumn(this.DesiredColumns);
            valid = valid && ImageBuilderUtils.ValidateScale(this.DesiredScale);
            valid = valid && ImageBuilderUtils.ValidateMargin(this.DesiredMarginX);
            valid = valid && ImageBuilderUtils.ValidateMargin(this.DesiredMarginY);

            return valid;
        }

        private int DesiredMarginX
        {
            get
            {
                int output = -1;

                try
                {
                    output = int.Parse(this.TextBoxMarginX.Text);
                }
                catch
                {
                    output = -1;
                }

                return output;
            }
            set
            {
                this.TextBoxMarginX.Text = "" + value;
            }
        }

        private int DesiredMarginY
        {
            get
            {
                int output = -1;

                try
                {
                    output = int.Parse(this.TextBoxMarginY.Text);
                }
                catch
                {
                    output = -1;
                }

                return output;
            }
            set
            {
                this.TextBoxMarginY.Text = "" + value;
            }
        }

        private Double DesiredScale
        {
            get
            {
                String doubleString;
                Double output = -1;

                try
                {
                    doubleString = this.TextBoxScale.Text;
                    output = ImageBuilderUtils.ParseScaleString(doubleString);
                }
                catch
                {
                    output = -1;
                }

                return output;
            }
            set
            {
                this.TextBoxScale.Text = ImageBuilderUtils.ConvertDoubleToString(value);
            }
        }

        private int DesiredLines
        {
            get
            {
                int output = -1;

                try
                {
                    output = int.Parse(this.TextBoxLineCount.Text);
                }
                catch
                {
                    output = -1;
                }

                return output;
            }
            set
            {
                this.TextBoxLineCount.Text = "" + value;
            }
        }

        private int DesiredColumns
        {
            get
            {
                int output = -1;

                try
                {
                    output = int.Parse(this.TextBoxColumnCount.Text);
                }
                catch
                {
                    output = -1;
                }

                return output;
            }
            set
            {
                this.TextBoxColumnCount.Text = "" + value;
            }
        }
    }
}
