﻿using System;
using System.Windows;
using System.Windows.Input;
using MediaReader.Core;

namespace MediaReader
{
    /// <summary>
    /// Logique d'interaction pour PlayListWindow.xaml
    /// </summary>
    public partial class PlayListWindow : Window, IMediaInterpret
    {

        private IMediaEventDispatcher mediaEventDispatcher;

        public PlayListWindow()
        {
            InitializeComponent();
        }

        void listSection_ItemDoubleClicked(int index)
        {
            this.mediaEventDispatcher.SelectFromPlaylistAt(index);
        }


        public int SelectedIndex
        {
            get
            {
                return this.LocalListView.SelectedIndex;
            }
        }

        public Boolean HasSelectedIndex
        {
            get
            {
                return this.SelectedIndex >= 0;
            }
        }


        #region handlers

        private void listSection_fileDropped(object sender, DragEventArgs e)
        {
            this.mediaEventDispatcher.ManagePlaylistDroppedUri(e);
        }

        private void LocalListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.HasSelectedIndex)
            {
                this.mediaEventDispatcher.SelectFromPlaylistAt(this.SelectedIndex);
            }
        }

        private void ListView_Drop(object sender, DragEventArgs e)
        {
            this.mediaEventDispatcher.ManagePlaylistDroppedUri(e);
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.HasSelectedIndex)
            {
                this.mediaEventDispatcher.SelectFromPlaylistAt(this.SelectedIndex);
            }
        }

        private void RemoveAllButton_Click(object sender, RoutedEventArgs e)
        {
            this.mediaEventDispatcher.PurgePlaylistRequest();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.HasSelectedIndex)
            {
                this.mediaEventDispatcher.DeleteFromPlaylistAt(this.SelectedIndex);
            }
        }

        #endregion

        #region MediaEventDispatcher
        public IMediaEventDispatcher LocalEventDispatcher
        {
            set
            {
                if (value != null)
                {
                    this.mediaEventDispatcher = value;
                    this.mediaEventDispatcher.ManagePlaylist(this.LocalListView);
                }
            }
            get
            {
                return this.mediaEventDispatcher;
            }
        }

        #endregion
    }
}
