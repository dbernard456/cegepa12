﻿using System;
using System.Windows;
using System.Windows.Controls;
using MediaReader.FileSystem;

namespace MediaReader.Core
{
    /// <summary>
    /// Interface qui sert à implémenter un déléguer de control des éléments média.
    /// 
    /// David Bernard
    /// </summary>
    public interface IMediaEventDispatcher
    {
        void ManageMediaUriRequest(Uri uri);
        void ManageDroppedUri(DragEventArgs e);
        void RequestApplicationClose();
        void RequestLaunchCaptureManager();

        // medias
        event EventHandler Tick;
        event EventHandler DesiredSpeedChanged;
        event EventHandler DesiredVolumeChanged;
        event EventHandler CurrentMediaStateChanged;
        event EventHandler CurrentMediaFileChanged;

        MediaElement CurrentMediaElement { get; }
        MediaState CurrentMediaState { get; }
        MediaFile CurrentMediaFile { get; }

        Boolean Paused { get; }
        Boolean Playing { get; }
        Boolean Stopped { get; }
        Boolean HasMediaFile { get; }
        Boolean HasTimeSpan { get; }

        Boolean PlayButtonShouldBeEnabled { get; }
        Boolean StopButtonShouldBeEnabled { get; }
        Boolean VolumeControlShouldBeEnabled { get; }
        Boolean SpeedControlShouldBeEnabled { get; }

        String PlayButtonText { get; }
        String CurrentDurationString { get; }
        String TotalDurationString { get; }
        String MediaFileLabel { get; }
        String VolumeIndicatorText { get; }
        String SpeedIndicatorText { get; }

        Double DurationSliderValue { get; }
        Double TotalDurationMilliseconds { get; }
        Double DesiredSpeed { get; }
        Double DesiredVolume { get; }

        void DeclareNewDesiredVolume(Double volume);
        void DeclareNewDesiredSpeed(Double speed);

        // Stop / Play buttons
        void ManagePlayButton();
        void ManageStopButton();

        // Duration Slider
        void DurationSliderStart();
        void DurationSliderDelta(int milliseconds);
        void DurationSliderStop();


        // Playlist
        event EventHandler ShowPlaylistWindow;

        Boolean PlaylistIsNew { get; }
        Boolean PlaylistIsDirty { get; }

        void ManagePlaylistDroppedUri(DragEventArgs e);
        void SavePlaylistRequest();
        void ManagePlaylistLoadRequest(Uri uri);
        void RequestShowPlaylistWindow();
        void ChangePlaylistPathRequest(String path);
        void ManagePlaylist(ListView listView);
        void PurgePlaylistRequest();
        void CreateNewPlaylistRequest();
        void DeleteFromPlaylistAt(int index);
        void SelectFromPlaylistAt(int index);
    }

    public interface IMediaInterpret
    {
        IMediaEventDispatcher LocalEventDispatcher { set; get; }
    }

}
