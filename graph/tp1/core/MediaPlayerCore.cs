﻿using System;
using System.Windows;
using System.Windows.Controls;
using MediaReader.FileSystem;
using MediaReader.Time;

namespace MediaReader.Core
{
    /// <summary>
    /// Classe qui controlle le programme.
    /// 
    /// David Bernard
    /// </summary>
    class MediaPlayerCore
    {
        public event EventHandler RequestClose;

        private readonly MediaCollection currentMediaStore;
        private readonly MediaConfig configs;
        private readonly MediaElement mediaElement;
        private readonly CoreMediaDispatcher mediaDispacher;
        private MediaFile currentMediaFile;
        private TimeManager timeManager;
        private MediaState currentMediaState;

        public MediaPlayerCore()
        {
            this.timeManager = new TimeManager();
            this.timeManager.TickEvent += new EventHandler(timeManager_TickEvent);

            this.configs = new MediaConfig(0);
            this.currentMediaStore = new MediaCollection();

            this.mediaDispacher = new CoreMediaDispatcher(this);

            this.timeManager.LocalEventDispatcher = this.Dispatcher;
            this.mediaElement = new MediaElement();
            this.mediaElement.ScrubbingEnabled = true;

            this.currentMediaState = MediaState.Stop;
        }

        public void ReadPersistantData()
        {
            try
            {
                this.configs.Read();
            }
            catch (Exception)
            {
                this.configs.RestoreDefaultValues();
            }

            this.CurrentMediaStore.LocalPath = this.configs.MediaStorePath;
            this.ReadStore();
        }

        #region handlers

        private void timeManager_TickEvent(Object sender, EventArgs e)
        {
            this.mediaDispacher.FireTick();
        }

        #endregion

        #region Properties

        private MediaCollection CurrentMediaStore
        {
            get
            {
                return this.currentMediaStore;
            }
        }

        public IMediaEventDispatcher Dispatcher
        {
            get
            {
                return this.mediaDispacher;
            }
        }


        public MediaFile CurrentMediaFile
        {
            get
            {
                return this.currentMediaFile;
            }
            set
            {
                this.CurrentMediaState = MediaState.Stop;
                this.currentMediaFile = value;
                this.mediaElement.Source = value.Uri;
                this.mediaDispacher.FireCurrentMediaFileChanged();
            }
        }

        public void LaunchCaputureManager()
        {
            CaptureWindow captureWindow;

            this.CurrentMediaState = MediaState.Stop;
            captureWindow = new CaptureWindow(this.configs, this.currentMediaFile);

            captureWindow.ShowDialog();
        }

        public MediaState CurrentMediaState
        {
            get
            {
                return this.currentMediaState;
            }
            set
            {
                this.currentMediaState = value;
                this.mediaElement.LoadedBehavior = value;
                this.mediaDispacher.FireCurrentMediaStateChanged();
            }

        }

        public Boolean Paused
        {
            get
            {
                return this.CurrentMediaState == MediaState.Pause;
            }
        }

        public Boolean Playing
        {
            get
            {
                return this.CurrentMediaState == MediaState.Play;
            }
        }

        public Boolean Stopped
        {
            get
            {
                return this.CurrentMediaState == MediaState.Stop;
            }
        }

        public String CurrentDurationString
        {
            get
            {
                String output = "-";

                if (this.HasTimeSpan)
                {
                    output = TimeUtils.FormatTimeSpan(this.mediaElement.Position);
                }

                return output;
            }
        }

        public String TotalDurationString
        {
            get
            {
                String output = "-";

                if (this.currentMediaFile != null)
                {
                    output = this.currentMediaFile.TotalDurationString;
                }
                return output;
            }
        }

        public Double DurationSliderValue
        {
            get
            {
                Double output = 0;

                if (this.HasTimeSpan)
                {
                    output = this.mediaElement.Position.TotalMilliseconds;
                }

                return output;
            }
        }

        public Boolean HasTimeSpan
        {
            get
            {
                return this.currentMediaFile != null && this.currentMediaFile.HasTimeSpan;
            }
        }

        #endregion

        #region IO methods

        public void ManagePlaylistLoadRequest(Uri uri)
        {
            if (uri != null)
            {
                this.currentMediaStore.LocalPath = uri.LocalPath;
                this.currentMediaStore.ReadMediaStore();
                this.configs.MediaStorePath = this.currentMediaStore.LocalPath;
            }
        }

        public void WriteStore()
        {
            this.CurrentMediaStore.WriteMediaStore();
        }

        public void ReadStore()
        {
            this.CurrentMediaStore.ReadMediaStore();
        }
        #endregion

        #region Media Store methods


        public void SelectFromPlayListAt(int index)
        {
            this.CurrentMediaFile = this.CurrentMediaStore.GetFileAt(index);
            this.CurrentMediaState = MediaState.Play;
        }

        #endregion

        #region Methods


        public void OpenMedia(Uri uri)
        {
            if (uri != null)
            {
                this.CurrentMediaFile = new MediaFile(uri);
            }
        }


        public void ManagePlayButton()
        {
            this.CurrentMediaState = this.Playing ? MediaState.Pause : MediaState.Play;

        }

        public void ManageStopButton()
        {
            this.CurrentMediaState = MediaState.Stop;
        }

        public void ManageDroppedUri(DragEventArgs e)
        {
            Uri[] uri = FileUtils.manageFileDrop(e);

            this.OpenMedia(uri[0]);

            // Start the new Media
            this.CurrentMediaState = MediaState.Play;
        }

        public void ManagePlaylistDroppedUri(DragEventArgs e)
        {
            MediaFile file;
            Uri[] uris = FileUtils.manageFileDrop(e);

            foreach (Uri uri in uris)
            {
                file = new MediaFile(uri);
                this.CurrentMediaStore.Add(file);
            }
        }

        private void ManageApplicationCloseReqest()
        {
            if (this.RequestClose != null)
            {
                this.RequestClose(this, null);
            }
        }

        public void ManageShutdown()
        {
            this.configs.Write();
        }

        public void DeclareNewDesiredVolume(Double volume)
        {
            this.mediaElement.Volume = volume;
            this.mediaDispacher.FireDesiredVolumeChanged();
        }

        public void DeclareNewDesiredSpeed(Double speed)
        {
            this.mediaElement.SpeedRatio = speed;
            this.mediaDispacher.FireDesiredSpeedChanged();
        }

        #endregion

        class CoreMediaDispatcher : IMediaEventDispatcher
        {
            #region Events
            public event EventHandler CurrentMediaStateChanged;
            public event EventHandler CurrentMediaFileChanged;
            public event EventHandler Tick;
            public event EventHandler DesiredSpeedChanged;
            public event EventHandler DesiredVolumeChanged;
            public event EventHandler ShowPlaylistWindow;
            #endregion

            private readonly MediaPlayerCore parent;

            public CoreMediaDispatcher(MediaPlayerCore parent)
            {
                this.parent = parent;
            }

            #region Properties
            public String VolumeIndicatorText
            {
                get
                {
                    return String.Format("{0:0.##}", (this.parent.mediaElement.Volume * 100)) + " %";
                }
            }

            public String SpeedIndicatorText
            {
                get
                {

                    return this.parent.mediaElement.SpeedRatio + " X";
                }
            }

            public Boolean PlaylistIsNew
            {
                get
                {
                    return this.parent.CurrentMediaStore.NewlyCreated;
                }
            }

            public Boolean PlaylistIsDirty
            {
                get
                {
                    return this.parent.currentMediaStore.Dirty;
                }
            }

            public MediaFile CurrentMediaFile
            {
                get
                {
                    return this.parent.CurrentMediaFile;
                }
            }
            public MediaState CurrentMediaState
            {
                get
                {
                    return this.parent.CurrentMediaState;
                }
            }

            public MediaCollection CurrentPlaylist
            {
                get
                {
                    return this.parent.currentMediaStore;
                }
            }

            public Boolean Paused
            {
                get
                {
                    return this.parent.Paused;
                }
            }

            public Boolean Playing
            {
                get
                {
                    return this.parent.Playing;
                }
            }

            public Boolean Stopped
            {
                get
                {
                    return this.parent.Stopped;
                }
            }

            public MediaElement CurrentMediaElement
            {
                get
                {
                    return this.parent.mediaElement;
                }
            }

            public Boolean HasMediaFile
            {
                get
                {
                    return this.CurrentMediaFile != null;
                }
            }

            public Boolean HasTimeSpan
            {
                get
                {
                    return this.parent.HasTimeSpan;
                }
            }

            public Boolean VolumeControlShouldBeEnabled
            {
                get
                {
                    return this.HasMediaFile;
                }
            }


            public Double DesiredSpeed
            {
                get
                {
                    return this.parent.mediaElement.SpeedRatio;
                }
            }

            public Double DesiredVolume
            {
                get
                {
                    return this.parent.mediaElement.Volume;
                }
            }

            public Double DurationSliderValue
            {
                get
                {
                    return this.parent.DurationSliderValue;
                }
            }


            public Double TotalDurationMilliseconds
            {
                get
                {
                    Double output = 0;

                    if (this.HasTimeSpan)
                    {
                        output = this.CurrentMediaFile.Duration.TotalMilliseconds;
                    }

                    return output;
                }
            }

            public Boolean SpeedControlShouldBeEnabled
            {
                get
                {
                    return this.HasMediaFile && this.HasTimeSpan;
                }
            }

            public Boolean PlayButtonShouldBeEnabled
            {
                get
                {
                    return this.HasMediaFile && this.HasTimeSpan;
                }
            }

            public Boolean StopButtonShouldBeEnabled
            {
                get
                {
                    return this.HasMediaFile && this.HasTimeSpan && (this.Playing || this.Paused);
                }
            }

            public String PlayButtonText
            {
                get
                {
                    return this.HasMediaFile && this.Playing ? "Pause" : "Play";
                }
            }

            public String CurrentDurationString
            {
                get
                {
                    return this.parent.CurrentDurationString;
                }
            }

            public String TotalDurationString
            {
                get
                {
                    return this.parent.TotalDurationString;
                }
            }

            public String MediaFileLabel
            {
                get
                {
                    String output = "";

                    if (this.HasMediaFile)
                    {
                        output = this.CurrentMediaFile.SimpleName;
                    }

                    return output;
                }
            }
            #endregion

            #region MediaState Methods

            public void ManagePlayButton()
            {
                this.parent.ManagePlayButton();
            }

            public void ManageStopButton()
            {
                this.parent.ManageStopButton();
            }

            #endregion

            #region Methods

            public void ManagePlaylist(ListView listView)
            {
                if (listView != null)
                {
                    listView.ItemsSource = this.parent.currentMediaStore;
                }
            }

            public void RequestLaunchCaptureManager()
            {
                this.parent.LaunchCaputureManager();
            }

            public void ManageMediaUriRequest(Uri uri)
            {
                this.parent.ManageStopButton();

                if (uri != null)
                {
                    this.parent.OpenMedia(uri);
                }
            }

            public void ManagePlaylistLoadRequest(Uri uri)
            {
                this.parent.ManagePlaylistLoadRequest(uri);

                this.RequestShowPlaylistWindow();
            }

            public void RequestShowPlaylistWindow()
            {
                if (this.ShowPlaylistWindow != null)
                {
                    this.ShowPlaylistWindow(this, null);
                }
            }

            public void ManageDroppedUri(DragEventArgs e)
            {
                this.parent.ManageDroppedUri(e);
            }

            public void ManagePlaylistDroppedUri(DragEventArgs e)
            {
                this.parent.ManagePlaylistDroppedUri(e);
            }

            public void DeclareNewDesiredVolume(Double volume)
            {
                this.parent.DeclareNewDesiredVolume(volume);
            }

            public void DeclareNewDesiredSpeed(Double speed)
            {
                this.parent.DeclareNewDesiredSpeed(speed);
            }

            public void RequestApplicationClose()
            {
                this.parent.ManageApplicationCloseReqest();
            }

            public void DurationSliderStart()
            {
                this.parent.mediaElement.LoadedBehavior = MediaState.Pause;
            }

            public void DurationSliderDelta(int miliseconds)
            {
                this.parent.mediaElement.Position = new TimeSpan(0, 0, 0, 0, miliseconds);
            }

            public void DurationSliderStop()
            {
                this.parent.mediaElement.LoadedBehavior = this.parent.CurrentMediaState;
            }

            public void SavePlaylistRequest()
            {
                if (this.parent.CurrentMediaStore.HasPath)
                {
                    this.parent.WriteStore();
                }
            }

            public void CreateNewPlaylistRequest()
            {
                this.CurrentPlaylist.Reset();
            }


            public void ChangePlaylistPathRequest(String path)
            {
                if (path != null)
                {
                    this.parent.CurrentMediaStore.LocalPath = path;
                    this.parent.configs.MediaStorePath = path;
                }
            }

            public void PurgePlaylistRequest()
            {
                this.parent.CurrentMediaStore.Clear();
            }

            public void DeleteFromPlaylistAt(int index)
            {
                this.parent.CurrentMediaStore.RemoveAt(index);
            }

            public void SelectFromPlaylistAt(int index)
            {
                this.parent.SelectFromPlayListAt(index);
            }

            #endregion

            #region Fire Event Methods
            public void FireDesiredSpeedChanged()
            {
                if (this.DesiredSpeedChanged != null)
                {
                    this.DesiredSpeedChanged(this, null);
                }
            }

            public void FireDesiredVolumeChanged()
            {
                if (this.DesiredVolumeChanged != null)
                {
                    this.DesiredVolumeChanged(this, null);
                }
            }

            public void FireCurrentMediaStateChanged()
            {
                if (this.CurrentMediaStateChanged != null)
                {
                    this.CurrentMediaStateChanged(this, null);
                }
            }

            public void FireCurrentMediaFileChanged()
            {
                if (this.CurrentMediaFileChanged != null)
                {
                    this.CurrentMediaFileChanged(this, null);
                }
            }



            public void FireTick()
            {
                if (this.Tick != null)
                {
                    this.Tick(this, null);
                }
            }

            #endregion
        }
    }
}
