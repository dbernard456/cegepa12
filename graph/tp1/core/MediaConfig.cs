﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;
using MediaReader.ImageBuilder;

namespace MediaReader.Core
{
    /// <summary>
    /// Classe qui gère la logique de configuration du programme.
    /// 
    /// David Bernard
    /// </summary>
    public class MediaConfig
    {
        private static readonly String CONFIG_PATH = "mediaConfigs.xml";
        private static readonly String DEFAULT_BACKGROUND = null;
        private static readonly int DEFAULT_LINE_COUNT = 5;
        private static readonly int DEFAULT_COLUMN_COUNT = 6;
        private static readonly double DEFAULT_SCALE = 0.6;
        private static readonly int DEFAULT_MARGIN_X = 20;
        private static readonly int DEFAULT_MARGIN_Y = 30;

        private String mediaStorePath;
        private String background;
        private double scale;
        private int lineCount;
        private int columnCount;
        private int marginX;
        private int marginY;

        public MediaConfig(int frameCount)
        {
            this.RestoreDefaultValues();
        }

        public void RestoreDefaultValues()
        {
            this.Scale = MediaConfig.DEFAULT_SCALE;
            this.Background = MediaConfig.DEFAULT_BACKGROUND;
            this.ColumnCount = MediaConfig.DEFAULT_COLUMN_COUNT;
            this.LineCount = MediaConfig.DEFAULT_LINE_COUNT;
            this.MarginX = MediaConfig.DEFAULT_MARGIN_X;
            this.MarginY = MediaConfig.DEFAULT_MARGIN_Y;
        }

        public int MarginX
        {
            get
            {
                return this.marginX;
            }
            set
            {
                if (ImageBuilderUtils.ValidateMargin(value))
                {
                    this.marginX = value;
                }
                else
                {
                    throw new Exception();
                }
            }
        }

        public int MarginY
        {
            get
            {
                return this.marginY;
            }
            set
            {
                if (ImageBuilderUtils.ValidateMargin(value))
                {
                    this.marginY = value;
                }
                else
                {
                    throw new Exception();
                }
            }
        }

        public String Background
        {
            get
            {
                return this.background;
            }
            set
            {
                this.background = value;
            }
        }

        public String MediaStorePath
        {
            get
            {
                return this.mediaStorePath;
            }
            set
            {
                this.mediaStorePath = value;
            }
        }

        public int LineCount
        {
            get
            {
                return this.lineCount;
            }
            set
            {
                if (ImageBuilderUtils.ValidateLine(value))
                {
                    this.lineCount = value;
                }
                else
                {
                    throw new Exception();
                }
            }
        }

        public int ColumnCount
        {
            get
            {
                return this.columnCount;
            }
            set
            {
                if (ImageBuilderUtils.ValidateColumn(value))
                {
                    this.columnCount = value;
                }
                else
                {
                    throw new Exception();
                }
            }
        }

        public Double Scale
        {
            get
            {
                return this.scale;
            }
            set
            {
                if (ImageBuilderUtils.ValidateScale(value))
                {
                    this.scale = value;
                }
                else
                {
                    throw new Exception();
                }
            }
        }


        public void Write()
        {
            XmlWriter xmlWriter = null;

            try
            {
                xmlWriter = XmlWriter.Create(MediaConfig.CONFIG_PATH);
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("mediaConfigs");

                xmlWriter.WriteStartElement("mediaStorePath");
                xmlWriter.WriteAttributeString("location", this.MediaStorePath);
                xmlWriter.WriteEndElement();


                xmlWriter.WriteStartElement("frameCapture");
                xmlWriter.WriteAttributeString("scale", "" + ImageBuilderUtils.ConvertDoubleToString(this.Scale));
                xmlWriter.WriteAttributeString("line", "" + this.LineCount);
                xmlWriter.WriteAttributeString("column", "" + this.ColumnCount);
                xmlWriter.WriteAttributeString("marginx", "" + this.MarginX);
                xmlWriter.WriteAttributeString("marginy", "" + this.MarginY);

                if (this.Background == null)
                {
                    xmlWriter.WriteAttributeString("background", "");
                }
                else
                {
                    xmlWriter.WriteAttributeString("background", this.Background);
                }

                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
            }
            finally
            {
                if (xmlWriter != null)
                {
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
            }

        }

        public void Read()
        {
            String name = null;
            String value = null;
            XmlReader reader = null;

            if (File.Exists(MediaConfig.CONFIG_PATH))
            {
                try
                {
                    reader = XmlReader.Create(MediaConfig.CONFIG_PATH);

                    while (reader.Read())
                    {
                        name = reader.Name;

                        if (name.Equals("mediaStorePath"))
                        {
                            value = reader.GetAttribute("location");
                            this.MediaStorePath = value;
                        }
                        else if (name.Equals("frameCapture"))
                        {
                            value = reader.GetAttribute("scale");
                            this.Scale = ImageBuilderUtils.ParseScaleString(value);

                            value = reader.GetAttribute("line");
                            this.LineCount = int.Parse(value);

                            value = reader.GetAttribute("column");
                            this.ColumnCount = int.Parse(value);

                            value = reader.GetAttribute("marginx");
                            this.MarginX = int.Parse(value);

                            value = reader.GetAttribute("marginy");
                            this.MarginY = int.Parse(value);

                            value = reader.GetAttribute("background");

                            if (value.Equals(""))
                            {
                                this.Background = null;
                            }
                            else
                            {
                                this.Background = value;
                            }

                        }
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
            }
        }
    }
}
