﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using MediaReader.Core;
using MediaReader.FileSystem;

namespace MediaReader
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MediaPlayerCore core;
        private PlayListWindow playListWindow;

        private IMediaEventDispatcher mediaEventDispatcher;

        public MainWindow()
        {
            InitializeComponent();

            this.core = new MediaPlayerCore();
            this.core.RequestClose += new EventHandler(core_RequestClose);
            this.playListWindow = null;

            this.Closing += new CancelEventHandler(MainWindow_Closing);
            this.Closed += new EventHandler(MainWindow_Closed);

            this.mediaEventDispatcher = this.core.Dispatcher;
            this.controlPanel.LocalEventDispatcher = this.core.Dispatcher;
            this.myMenuBar.LocalEventDispatcher = this.core.Dispatcher;

            DockPanel.SetDock(this.mediaEventDispatcher.CurrentMediaElement, Dock.Top);
            this.DockPan.Children.Add(this.mediaEventDispatcher.CurrentMediaElement);

            this.core.ReadPersistantData();

            this.PrepareForDisplay();

            if (App.CommandLineParam != null)
            {
                this.mediaEventDispatcher.ManageMediaUriRequest(new Uri(App.CommandLineParam));
            }
        }

        private void core_RequestClose(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DockPanel_Drop(object sender, DragEventArgs e)
        {
            this.mediaEventDispatcher.ManageDroppedUri(e);
        }

        void MainWindow_Closed(object sender, EventArgs e)
        {
            if (this.playListWindow != null)
            {
                this.playListWindow.Close();
            }

            this.core.ManageShutdown();
        }

        #region handlers
        void playListWindow_Closed(object sender, System.EventArgs e)
        {
            this.playListWindow = null;
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            String path;
            MessageBoxResult rsltMessageBox;
            IMediaEventDispatcher mediaEventDispatcher = this.core.Dispatcher;

            if (mediaEventDispatcher.PlaylistIsDirty)
            {
                rsltMessageBox = MessageBox.Show("Do you want to save the current Playlist", "Playlist", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                if (rsltMessageBox == MessageBoxResult.Yes)
                {
                    if (mediaEventDispatcher.PlaylistIsNew)
                    {
                        path = MediaCollection.CreateStoreFile();
                        mediaEventDispatcher.ChangePlaylistPathRequest(path);
                    }

                    mediaEventDispatcher.SavePlaylistRequest();

                    if (this.playListWindow != null)
                    {
                        this.playListWindow.Close();
                    }
                }
                else if (rsltMessageBox == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.playListWindow.Show();
        }

        #endregion

        public void PrepareForDisplay()
        {
            this.mediaEventDispatcher.ShowPlaylistWindow += new EventHandler(mediaEventDispatcher_ShowPlaylistWindow);
            this.CreateNewPlaylistWindow();
        }

        private void CreateNewPlaylistWindow()
        {
            if (this.playListWindow == null)
            {
                this.playListWindow = new PlayListWindow();
                this.playListWindow.LocalEventDispatcher = this.mediaEventDispatcher;
                this.playListWindow.Closed += new System.EventHandler(playListWindow_Closed);
            }

            this.playListWindow.Show();
            this.playListWindow.Activate();
        }

        #region MediaEventDispatcher


        void mediaEventDispatcher_ShowPlaylistWindow(Object sender, EventArgs e)
        {
            this.CreateNewPlaylistWindow();
        }

        #endregion
    }
}


// http://www.wpftutorial.net/WrapPanel.html