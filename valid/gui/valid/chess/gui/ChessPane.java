
package valid.chess.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import valid.chess.GuiDelegate;
import valid.chess.board.Board;
import valid.chess.board.BoardNode;
import valid.chess.board.BoardPosition;

/**
 * Classe qui représente le grillage du jeu d'échec.
 * 
 * @author David Bernard
 * @author Danny Dombrowski
 * 
 */
public class ChessPane extends JPanel
{
	private static final long serialVersionUID = -8700810188023830375L;

	/**
	 * Détermine la taille d'un carré du plateau.
	 */
	public static final int SQUARE_SIZE = 100;

	/**
	 * Determine la taille de la marge autour du plateau.
	 */
	public static final int MARGIN_SIZE = 50;

	/**
	 * Écouteur de souris.
	 */
	private final MouseListener mouseListener;

	private final Rectangle[][] rectangles;
	private final Rectangle borders;
	private GuiDelegate guiDelegate;

	/**
	 * Constructeur
	 * 
	 */
	public ChessPane()
	{
		Rectangle r;
		int x;
		int y;

		this.rectangles = new Rectangle[Board.DIMENSION][Board.DIMENSION];

		for (int i = 0; i < this.rectangles.length; i++)
		{
			for (int j = 0; j < this.rectangles[i].length; j++)
			{
				x = (i * ChessPane.SQUARE_SIZE) + ChessPane.MARGIN_SIZE;
				y = (j * ChessPane.SQUARE_SIZE) + ChessPane.MARGIN_SIZE;

				r = new Rectangle(x, y, ChessPane.SQUARE_SIZE, ChessPane.SQUARE_SIZE);
				this.rectangles[i][j] = r;
			}
		}

		this.borders = new Rectangle(new Point(0, 0), this.getSize());

		this.setPreferredSize(this.getBoardSize());

		this.mouseListener = new MyMouseListener();
		this.addMouseListener(this.mouseListener);
		this.setBackground(Color.BLUE);
	}

	/**
	 * Donne les dimensions du plateau.
	 * 
	 * @return un objet dimensiom qui représente les dimension du plateau.
	 */
	public final Dimension getBoardSize()
	{
		int totalDim;

		totalDim = 0;

		totalDim += Board.DIMENSION * ChessPane.SQUARE_SIZE;
		totalDim += 2 * ChessPane.MARGIN_SIZE;

		return new Dimension(totalDim, totalDim);
	}

	/**
	 * Mutateur du délégé de l'interface graphique.
	 * 
	 * @param delegate
	 */
	public void setGuiDelegate(GuiDelegate delegate)
	{
		this.guiDelegate = delegate;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		Rectangle rect;
		super.paintComponent(g);

		for (int i = 0; i < this.rectangles.length; i++)
		{
			for (int j = 0; j < this.rectangles[i].length; j++)
			{
				rect = this.rectangles[i][j];

				g.setColor(BoardNode.colorForCoordinates(i, j));
				g.fillRect(rect.x, rect.y, rect.height, rect.width);
			}
		}
	}

	public BoardPosition seekBoardPositionAtPoint(Point p)
	{
		Rectangle rect;
		BoardPosition output;

		output = null;

		for (int i = 0; i < this.rectangles.length && output == null; i++)
		{
			for (int j = 0; j < this.rectangles[i].length && output == null; j++)
			{
				rect = this.rectangles[i][j];

				if (rect.contains(p))
				{
					System.out.println("i " + i + " j " + j);
					output = new BoardPosition(i, j);
				}
			}
		}

		return output;
	}

	/**
	 * Ecouteur de souris local.
	 * 
	 * @author Danny Dombroswki
	 * @author David Bernard
	 */
	private class MyMouseListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e)
		{
			BoardPosition position = ChessPane.this.seekBoardPositionAtPoint(e.getPoint());
			System.out.println(position);
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}
	}
}
