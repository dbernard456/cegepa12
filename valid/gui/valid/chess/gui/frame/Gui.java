
package valid.chess.gui.frame;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import valid.chess.ChessGui;
import valid.chess.GuiDelegate;
import valid.chess.gui.ChessPane;

/**
 * Interface graphique.
 * 
 * @author Danny Dombroswki
 * @author David Bernard
 */
public class Gui extends JFrame implements ChessGui
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7445658034584942875L;

	/**
	 * Instance du panneau d'échec
	 */
	private final ChessPane chessPane;

	/**
	 * Délégé qui assure le lien entre la vue et le controlleur.
	 */
	private GuiDelegate guiDelegate;

	/**
	 * Constructeur de l'interface graphique.
	 */
	public Gui()
	{
		super();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.chessPane = new ChessPane();
		this.add(this.chessPane, BorderLayout.CENTER);

		this.setSize(this.chessPane.getBoardSize());
		
		this.pack();
	}

	/**
	 * Mutateur du délégé de l'interface graphique.
	 * 
	 * @param delegate
	 */
	@Override
	public void setGuiDelegate(GuiDelegate delegate)
	{
		this.guiDelegate = delegate;
	}

	private class ExitAction extends AbstractAction
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			// TODO Auto-generated method stub

		}

	}

}
