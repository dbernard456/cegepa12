
package valid.chess.gui.image;

import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;

import valid.chess.Chess;

/**
 * Implemente la facade qui fournit les images de pieces.
 * 
 * @author David Bernard
 * @author Danny Dombroski
 */
public class BinaryImageProvider extends PieceImageProvider
{
	@Override
	public Image fetchRook(Color color) throws IOException, URISyntaxException
	{
		return this.generateImage("rook", color);
	}

	@Override
	public Image fetchBishop(Color color) throws IOException, URISyntaxException
	{
		return this.generateImage("bishop", color);
	}

	@Override
	public Image fetchKnight(Color color) throws IOException, URISyntaxException
	{
		return this.generateImage("knight", color);
	}

	@Override
	public Image fetchKing(Color color) throws IOException, URISyntaxException
	{
		return this.generateImage("king", color);
	}

	@Override
	public Image fetchQueen(Color color) throws IOException, URISyntaxException
	{
		return this.generateImage("queen", color);
	}

	@Override
	public Image fetchPawn(Color color) throws IOException, URISyntaxException
	{
		return this.generateImage("pawn", color);
	}

	private String colorToString(Color color)
	{
		return (color.equals(Chess.PLAYER_WHITE)) ? "white" : "black";
	}

	private Image generateImage(String file, Color color) throws IOException, URISyntaxException
	{
		if (!Chess.validateColor(color))
		{
			throw new RuntimeException("Couleur de pièce invalide");
		}

		return ImageIO.read(new File(this.getClass().getResource("/pieces/" + this.colorToString(color) + "/" + file + ".png").toURI()));
	}
}
