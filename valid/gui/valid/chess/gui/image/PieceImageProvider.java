
package valid.chess.gui.image;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;
import java.net.URISyntaxException;

public abstract class PieceImageProvider
{
	public abstract Image fetchRook(Color color) throws IOException, URISyntaxException;

	public abstract Image fetchBishop(Color color) throws IOException, URISyntaxException;

	public abstract Image fetchKnight(Color color) throws IOException, URISyntaxException;

	public abstract Image fetchKing(Color color) throws IOException, URISyntaxException;

	public abstract Image fetchQueen(Color color) throws IOException, URISyntaxException;

	public abstract Image fetchPawn(Color color) throws IOException, URISyntaxException;
}
