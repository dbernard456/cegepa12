
package gui;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import valid.chess.Chess;
import valid.chess.gui.image.BinaryImageProvider;

@SuppressWarnings("javadoc")
public class ImagePiece extends JFrame
{
	private static final long serialVersionUID = 1L;
	private Image pieceImg = null;

	public ImagePiece() throws IOException, URISyntaxException
	{
		JPanel panel = null;
		BinaryImageProvider img = null;

		// On genere l'image
		img = new BinaryImageProvider();
		this.pieceImg = img.fetchKnight(Chess.PLAYER_WHITE);

		panel = new JPanel()
		{
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				g.drawImage(ImagePiece.this.pieceImg, 0, 0, null);
			}
		};

		this.add(panel);

		// Le JFrame
		this.setSize(300, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	public static void main(String[] args)
	{
		try
		{
			new ImagePiece();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (URISyntaxException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
