
package valid.chess.board;

import java.awt.Color;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import valid.chess.Chess;
import valid.chess.piece.Bishop;
import valid.chess.piece.King;
import valid.chess.piece.Knight;
import valid.chess.piece.Pawn;
import valid.chess.piece.Piece;
import valid.chess.piece.Queen;
import valid.chess.piece.Rook;

@SuppressWarnings("javadoc")
public class BoardTest
{
	private Board board;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		this.board = new Board();
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testInitializeBoard()
	{
		Color currentColor;
		int i;
		int j;

		this.board.initializeBoard();

		// Tester les noirs.
		currentColor = Color.BLACK;

		i = 0;
		j = 0;

		BoardTest.assertPiece(i, j++, this.board, Rook.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Knight.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Bishop.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Queen.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, King.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Knight.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Bishop.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Rook.class, currentColor);

		for (i = 1, j = 0; j < Board.DIMENSION; j++)
		{
			BoardTest.assertPiece(i, j++, this.board, Pawn.class, currentColor);
		}

		// Tester le centre
		for (i = 2; i < 6; i++)
		{
			for (j = 0; j < Board.DIMENSION; j++)
			{
				BoardTest.assertPiece(i, j, this.board, null, null);
			}
		}

		// Tester les blancs.
		currentColor = Color.WHITE;
		for (i = 6, j = 0; j < Board.DIMENSION; j++)
		{
			BoardTest.assertPiece(i, j++, this.board, Pawn.class, currentColor);
		}

		i = 7;
		j = 0;

		BoardTest.assertPiece(i, j++, this.board, Rook.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Knight.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Bishop.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Queen.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, King.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Knight.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Bishop.class, currentColor);
		BoardTest.assertPiece(i, j++, this.board, Rook.class, currentColor);
	}

	@Test
	public void testGetCoordinateAt()
	{
		for (int i = 0; i < Board.DIMENSION; i++)
		{
			for (int j = 0; j < Board.DIMENSION; j++)
			{
				Assert.assertTrue(Board.getCoordinateExists(i, j));
			}
		}

		Assert.assertFalse(Board.getCoordinateExists(Board.DIMENSION, Board.DIMENSION));
		Assert.assertFalse(Board.getCoordinateExists(Board.DIMENSION + 1, Board.DIMENSION + 1));
		Assert.assertFalse(Board.getCoordinateExists(Board.DIMENSION + 3, Board.DIMENSION + 6));

		Assert.assertFalse(Board.getCoordinateExists(-1, -1));
		Assert.assertFalse(Board.getCoordinateExists(-3, -5));
	}

	@Test
	public void testCheckCheckmate()
	{
		this.board.initializeBoard();
		Assert.assertTrue(this.board.checkCheckmate() == null);

		this.board.clearBoard();

		this.board.setPieceAt(7, 3, new King(Chess.PLAYER_BLACK));
		this.board.setPieceAt(5, 3, new King(Chess.PLAYER_WHITE));
		this.board.setPieceAt(7, 6, new Rook(Chess.PLAYER_WHITE));

		Assert.assertTrue(this.board.checkCheckmate() == Chess.PLAYER_BLACK);
		Assert.assertFalse(this.board.checkStalemate(Chess.PLAYER_BLACK));
		Assert.assertFalse(this.board.checkStalemate(Chess.PLAYER_WHITE));

		this.board.initializeBoard();
		this.board.setPieceAt(7, 3, new King(Chess.PLAYER_BLACK));
		this.board.setPieceAt(0, 3, new King(Chess.PLAYER_WHITE));
		Assert.assertTrue(this.board.checkCheckmate() == null);
	}

	@Test
	public void testCheckStalemate()
	{
		this.board.initializeBoard();
		Assert.assertFalse(this.board.checkStalemate(Chess.PLAYER_BLACK));

		this.board.clearBoard();

		// Configurer le plateau pour provoque un Pat.
		this.board.setPieceAt(0, 7, new King(Chess.PLAYER_WHITE));
		this.board.setPieceAt(2, 6, new Queen(Chess.PLAYER_BLACK));
		this.board.setPieceAt(2, 7, new Pawn(Chess.PLAYER_BLACK));
		this.board.setPieceAt(3, 6, new Pawn(Chess.PLAYER_BLACK));
		this.board.setPieceAt(4, 5, new Pawn(Chess.PLAYER_BLACK));
		this.board.setPieceAt(7, 6, new King(Chess.PLAYER_BLACK));

		Assert.assertTrue(this.board.checkStalemate(Chess.PLAYER_WHITE));
		Assert.assertTrue(this.board.checkCheckmate() == null);
	}

	@Test
	public void testGetPieceCount()
	{
		this.board.initializeBoard();
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_BLACK) == 16);
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_WHITE) == 16);

		this.board.setPieceAt(0, 0, null);
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_WHITE) == 15);

		this.board.setPieceAt(0, 0, new Pawn(Chess.PLAYER_WHITE));
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_WHITE) == 16);

		this.board.clearBoard();
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_BLACK) == 0);
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_WHITE) == 0);

		this.board.initializeBoard();
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_BLACK) == 16);
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_WHITE) == 16);
	}

	@Test
	public void clearBoard()
	{
		this.board.initializeBoard();

		this.board.clearBoard();
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_BLACK) == 0);
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_WHITE) == 0);

		this.board.initializeBoard();
		this.board.clearBoard();
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_BLACK) == 0);
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_WHITE) == 0);

		this.board.setPieceAt(0, 0, new Pawn(Chess.PLAYER_WHITE));
		this.board.clearBoard();
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_BLACK) == 0);
		Assert.assertTrue(this.board.getPieceCount(Chess.PLAYER_WHITE) == 0);

	}

	private static boolean assertPiece(int i, int j, Board board, Class<? extends Piece> expectedPiece, Color expectedColor)
	{
		Piece foundPiece;
		boolean success;

		success = Board.getCoordinateExists(i, j);
		foundPiece = board.getPieceAt(i, j);

		if (success)
		{
			if ((expectedPiece == null) && (foundPiece == null))
			{
				success = true;
			}
			else if ((expectedPiece != null) && (foundPiece != null) && (expectedColor != null))
			{
				success = expectedPiece.equals(foundPiece.getClass());
				success = success && foundPiece.getColor().equals(expectedColor);
			}
			else
			{
				success = false;
			}
		}

		return success;
	}

}
