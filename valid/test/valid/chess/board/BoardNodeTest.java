
package valid.chess.board;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import valid.chess.Chess;
import valid.chess.piece.Piece;
import valid.chess.piece.Rook;

@SuppressWarnings("javadoc")
public class BoardNodeTest
{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testBoardNode()
	{
		Assert.fail("Not yet implemented");
	}

	@Test
	public void testGetI()
	{
		int testValue;
		BoardNode node;

		testValue = 4;
		node = new BoardNode(testValue, 2);
		Assert.assertTrue(node.getI() == testValue);

		testValue = 7;
		node = new BoardNode(testValue, 2);
		Assert.assertTrue(node.getI() == testValue);

		testValue = 7;
		node = new BoardNode(testValue, testValue);
		Assert.assertTrue(node.getI() == testValue);

		testValue = 7;
		node = new BoardNode(testValue, 0);
		Assert.assertTrue(node.getI() == testValue);
	}

	@Test
	public void testGetJ()
	{
		int testValue;
		BoardNode node;

		testValue = 4;
		node = new BoardNode(2, testValue);
		Assert.assertTrue(node.getI() == testValue);

		testValue = 7;
		node = new BoardNode(4, testValue);
		Assert.assertTrue(node.getI() == testValue);

		testValue = 7;
		node = new BoardNode(testValue, testValue);
		Assert.assertTrue(node.getI() == testValue);

		testValue = 7;
		node = new BoardNode(0, testValue);
		Assert.assertTrue(node.getI() == testValue);
	}

	@Test
	public void testGetCurrentPiece()
	{
		BoardNode node;
		Piece piece;
		Piece foundPiece;

		piece = new Rook(Chess.PLAYER_BLACK);
		node = new BoardNode(3, 3);

		node.setCurrentPiece(piece);

		foundPiece = node.getCurrentPiece();

		Assert.assertFalse(foundPiece == null);
		Assert.assertTrue(foundPiece == piece);
	}

	@Test
	public void testSetCurrentPiece()
	{
		BoardNode node;
		Piece piece;
		Piece foundPiece;

		piece = new Rook(Chess.PLAYER_BLACK);
		node = new BoardNode(3, 3);

		node.setCurrentPiece(piece);

		foundPiece = node.getCurrentPiece();

		Assert.assertFalse(foundPiece == null);
		Assert.assertTrue(foundPiece == piece);
	}

	@Test
	public void testGetBoardPosition()
	{
		Assert.fail("Not yet implemented");
	}

	@Test
	public void testColorForCoordinates()
	{
		Assert.fail("Not yet implemented");
	}

}
