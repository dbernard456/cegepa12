
package valid.chess.piece;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import valid.chess.Chess;
import valid.chess.board.Board;
import valid.chess.board.BoardPosition;

@SuppressWarnings("javadoc")
public class PawnTest
{
	private Pawn black = null;
	private Pawn white = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		this.black = new Pawn(Chess.PLAYER_BLACK);
		this.white = new Pawn(Chess.PLAYER_WHITE);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testGenerateAllowedMoves()
	{
		BoardPosition currentPosition = null;
		ArrayList<BoardPosition> positions = null;

		// Test pour les pawns blancs
		currentPosition = new BoardPosition(1, 0);
		positions = this.white.generateAllowedMoves(currentPosition);

		for (BoardPosition p : positions)
		{
			if (!Board.getCoordinateExists(p))
			{
				Assert.fail("Position invalide - La piece est sortie du board");
			}

			if ((currentPosition.getI() == p.getI()) && (currentPosition.getJ() == p.getJ()))
			{
				Assert.fail("Il y a eu aucun déplacement");
			}

			if (p.getI() < currentPosition.getI())
			{
				Assert.fail("Position i invalide - La piece a reculer");
			}

			if ((currentPosition.getI() != 1) && ((currentPosition.getI() + 1) != p.getI()))
			{
				Assert.fail("La piece a avancer de 2 positions ou plus lorsqu'elle n'est pas a sont emplacement de depart");
			}

			if ((p.getI() == currentPosition.getI()) && (p.getJ() == currentPosition.getJ()))
			{
				Assert.fail("Position j invalide - La piece ne peux pas ce deplacer uniquement de coter");
			}

			if ((currentPosition.getJ() != p.getJ()) || ((currentPosition.getJ() + 1) != p.getJ()) || ((currentPosition.getJ() - 1) != p.getJ()))
			{
				Assert.fail("Position j invalide - La piece a faite un déplacement en j invalide");
			}
		}

		// Test pour les pawns noirs
		currentPosition = new BoardPosition(6, 0);
		positions = this.black.generateAllowedMoves(currentPosition);

		for (BoardPosition p : positions)
		{
			if (p.getI() < 0)
			{
				Assert.fail("Position i invalide - La piece est sortie du board");
			}

			if ((currentPosition.getI() == p.getI()) && (currentPosition.getJ() == p.getJ()))
			{
				Assert.fail("Il y a eu aucun déplacement");
			}

			if (p.getI() > currentPosition.getI())
			{
				Assert.fail("Position i invalide - La piece a reculer");
			}

			if ((currentPosition.getI() != 6) && ((currentPosition.getI() - 1) != p.getI()))
			{
				Assert.fail("La piece a avancer de 2 positions ou plus lorsqu'elle n'est pas a sont emplacement de depart");
			}

			if ((p.getJ() > (Board.DIMENSION - 1)) || (p.getJ() <= 0))
			{
				Assert.fail("Position i invalide - La piece est sortie du board");
			}

			if ((p.getI() == currentPosition.getI()) && (p.getJ() == currentPosition.getJ()))
			{
				Assert.fail("Position j invalide - La piece ne peux ce deplacer de coter");
			}

			if ((currentPosition.getJ() != p.getJ()) || ((currentPosition.getJ() + 1) != p.getJ()) || ((currentPosition.getJ() - 1) != p.getJ()))
			{
				Assert.fail("Position j invalide - La piece a faite un déplacement en j invalide");
			}
		}
	}

}
