
package valid.chess.piece;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import valid.chess.Chess;
import valid.chess.board.Board;
import valid.chess.board.BoardPosition;

@SuppressWarnings("javadoc")
public class BishopTest
{
	private Bishop piece = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		this.piece = new Bishop(Chess.PLAYER_WHITE);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testGenerateAllowedMoves()
	{
		boolean positionValide = false;

		BoardPosition currentPosition = null;
		ArrayList<BoardPosition> positions = null;

		// Test pour les bishop
		currentPosition = new BoardPosition(0, 2);
		positions = this.piece.generateAllowedMoves(currentPosition);

		for (BoardPosition p : positions)
		{
			if (!Board.getCoordinateExists(p))
			{
				Assert.fail("La piece est a l'extérieur du board");
			}

			if (p.getI() < currentPosition.getI())
			{
				Assert.fail("Position i invalide - La piece a reculer");
			}

			for (int i = 1; (i <= (Board.DIMENSION - 1)) && !positionValide; i++)
			{
				if ((currentPosition.getI() == (p.getI() + i)) && (currentPosition.getJ() == (p.getJ() + i)))
				{
					positionValide = true;
				}

				if (!Board.getCoordinateExists(p.getI() + i, p.getJ() + i) && !Board.getCoordinateExists(p.getI() + i, p.getJ() - i) && !Board.getCoordinateExists(p.getI() - i, p.getJ() + i)
						&& !Board.getCoordinateExists(p.getI() - i, p.getJ() - i))
				{
					Assert.fail("Les coordonnées i - j n'existe plus et une position valide n'a pas été trouvé");
				}
			}

			positionValide = false;
		}
	}
}
