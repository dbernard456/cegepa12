
package valid.chess.piece;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import valid.chess.Chess;
import valid.chess.board.Board;
import valid.chess.board.BoardPosition;

@SuppressWarnings("javadoc")
public class KnightTest
{
	private Knight piece = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		this.piece = new Knight(Chess.PLAYER_WHITE);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testGenerateAllowedMoves()
	{
		BoardPosition currentPosition = null;
		ArrayList<BoardPosition> positions = null;

		// Test pour les pawns blancs
		currentPosition = new BoardPosition(0, 1);
		positions = this.piece.generateAllowedMoves(currentPosition);

		for (BoardPosition p : positions)
		{
			if (!Board.getCoordinateExists(p))
			{
				Assert.fail("La piece est a l'extérieur du board");
			}

			if ((currentPosition.getI() == p.getI()) && (currentPosition.getJ() == p.getJ()))
			{
				Assert.fail("Il y a eu aucun déplacement");
			}

			if (p.getI() == (currentPosition.getI() + 1))
			{
				if ((p.getJ() != (currentPosition.getJ() + 2)) && (p.getJ() != (currentPosition.getJ() - 2)))
				{
					Assert.fail("Déplacement en J invalide lors d'un déplacement en i de 1");
				}
			}
			else if (p.getI() == (currentPosition.getI() + 1))
			{
				if ((p.getJ() != (currentPosition.getJ() + 1)) && (p.getJ() != (currentPosition.getJ() - 1)))
				{
					Assert.fail("Déplacement en J invalide lors d'un déplacement en i de 2");
				}
			}
			else
			{
				Assert.fail("Déplacement en I invalide.");
			}
		}
	}

}
