
package valid.chess.piece;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import valid.chess.Chess;
import valid.chess.board.Board;
import valid.chess.board.BoardPosition;

@SuppressWarnings("javadoc")
public class KingTest
{
	private King piece = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		this.piece = new King(Chess.PLAYER_WHITE);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testGenerateAllowedMoves()
	{
		BoardPosition currentPosition = null;
		ArrayList<BoardPosition> positions = null;

		// Test pour les pawns blancs
		currentPosition = new BoardPosition(0, 4);
		positions = this.piece.generateAllowedMoves(currentPosition);

		for (BoardPosition p : positions)
		{
			if (!Board.getCoordinateExists(p))
			{
				Assert.fail("La piece est a l'extérieur du board");
			}

			if ((currentPosition.getI() == p.getI()) && (currentPosition.getJ() == p.getJ()))
			{
				Assert.fail("Il y a eu aucun déplacement");
			}

			if (((currentPosition.getI() - p.getI()) > 1) || ((currentPosition.getI() - p.getI()) < -1))
			{
				Assert.fail("Déplacement en i invalide");
			}

			if (((currentPosition.getJ() - p.getJ()) > 1) || ((currentPosition.getJ() - p.getJ()) < -1))
			{
				Assert.fail("Déplacement en j invalide");
			}
		}
	}

}
