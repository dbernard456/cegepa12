
package valid.chess.piece;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import valid.chess.Chess;
import valid.chess.board.Board;
import valid.chess.board.BoardPosition;

@SuppressWarnings("javadoc")
public class RookTest
{
	private Rook piece = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		this.piece = new Rook(Chess.PLAYER_WHITE);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testGenerateAllowedMoves()
	{
		BoardPosition currentPosition = null;
		ArrayList<BoardPosition> positions = null;

		// Test pour les pawns blancs
		currentPosition = new BoardPosition(0, 0);
		positions = this.piece.generateAllowedMoves(currentPosition);

		for (BoardPosition p : positions)
		{
			Assert.assertFalse("La piece est a l'extérieur du board", Board.getCoordinateExists(p));

			if ((currentPosition.getI() == p.getI()) && (currentPosition.getJ() == p.getJ()))
			{
				Assert.fail("Il y a eu aucun déplacement");
			}

			if ((p.getI() != currentPosition.getI()) || (p.getJ() != currentPosition.getJ()))
			{
				Assert.fail("La piece a faite un deplacement en i et en j");
			}
		}
	}

}
