
package valid.chess.piece;

import java.awt.Color;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import valid.chess.Chess;

@SuppressWarnings("javadoc")
public class PieceTest
{
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testPiece()
	{
		try
		{
			new Pawn(Color.BLUE);
			Assert.fail("Couleur invalide passer en paramètre et aucune exception");
		}
		catch (Exception e)
		{
		}

		try
		{
			new Pawn(Chess.PLAYER_BLACK);
		}
		catch (Exception e)
		{
			Assert.fail("Couleur valide du player noir passer en paramètre et ça lève une exception");
		}

		try
		{
			new Pawn(Chess.PLAYER_WHITE);
		}
		catch (Exception e)
		{
			Assert.fail("Couleur valide du player blanc passer en paramètre et ça lève une exception");
		}
	}

	@Test
	public void getMoveCountTest()
	{
		Piece piece = new Pawn(Chess.PLAYER_WHITE);

		if (piece.getMoveCount() != 0)
		{
			Assert.fail("Le move count n'est pas initialisé à 0");
		}
	}

	public void incrementMoveTest()
	{
		Piece piece = new Pawn(Chess.PLAYER_WHITE);

		piece.incrementMove();

		if (piece.getMoveCount() != 1)
		{
			Assert.fail("Le move count n'a pas été incrémenté");
		}
	}

}
