
package valid.chess.board;

public class BoardPosition
{
	private final int i;
	private final int j;

	public BoardPosition(final int i, final int j)
	{
		boolean ok;

		ok = Board.getCoordinateExists(i, j);

		if (!ok)
		{
			throw new RuntimeException();
		}

		this.i = i;
		this.j = j;
	}

	public int getI()
	{
		return this.i;
	}

	public int getJ()
	{
		return this.j;
	}

	@Override
	public String toString()
	{
		return "<i=" + this.i + " j=" + this.j + ">";
	}
}
