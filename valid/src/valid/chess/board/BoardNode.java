
package valid.chess.board;

import java.awt.Color;

import valid.chess.Chess;
import valid.chess.piece.Piece;

public class BoardNode extends BoardPosition
{
	private final int i;
	private final int j;

	private Piece piece;
	private Color color;

	public BoardNode(final int i, final int j)
	{
		super(i, j);

		// Post Conditions
		// TODO create post conditions

		assert this.invariant();

		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public Piece getCurrentPiece()
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public void setCurrentPiece(Piece piece)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public BoardPosition getBoardPosition()
	{
		return new BoardPosition(this.getI(), this.getJ());
	}

	private boolean invariant()
	{
		boolean success = true;

		success = success && (this.color != null);
		success = success && (this.color == BoardNode.colorForCoordinates(this.i, this.j));

		return success;
	}

	public static Color colorForCoordinates(final int i, final int j)
	{
		Color output;

		if ((((i % 2) == 0) && ((j % 2) == 0)) || (((i % 2) == 1) && ((j % 2) == 1)))
		{
			output = Chess.PLAYER_WHITE;
		}
		else
		{
			output = Chess.PLAYER_BLACK;
		}

		return output;
	}
}
