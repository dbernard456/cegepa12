
package valid.chess.board;

import java.awt.Color;
import java.util.ArrayList;

import valid.chess.piece.Piece;

/**
 * Représent le plateau du jeu d'échec.
 * 
 * @author David Bernard
 * @author Danny Dombrowski
 * 
 * @since 1.6
 */
public class Board
{
	public final static int DIMENSION = 8;

	private BoardNode selectedNode;
	private BoardNode[][] matrix;
	private BoardNode[] array;

	public Board()
	{
		// TODO implement.

		// Post Conditions
		// TODO create post conditions

		assert this.invariant();
		throw new UnsupportedOperationException();
	}

	/**
	 * Prepare le plateur d'echec
	 */
	public void initializeBoard()
	{
		this.clearBoard();

		assert this.invariant();
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public Piece getPieceAt(int i, int j)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public void clearBoard()
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public Color getColorAt(int i, int j)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public Color CheckCheck()
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public Color checkCheckmate()
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public boolean checkStalemate(Color currentPlayer)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	private boolean invariant()
	{
		final int totalExpectedNodes = Board.DIMENSION * Board.DIMENSION;
		boolean result = true;

		result = result && (this.array != null) && (this.array.length == totalExpectedNodes);
		result = result && (this.matrix != null) && (this.matrix.length == totalExpectedNodes);

		return result;
	}

	public Piece[] fetchRemainingPieces()
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public void trimMovesList(ArrayList<BoardPosition> moves)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public BoardPosition getKingPositionForPlayer(Color playerColor)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public int getPieceCount(Color player)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	/**
	 * Move a piece from one position to another.
	 * 
	 * @param from start Postion.
	 * @param to destination.
	 * @return An killed enemy piece or null if none.
	 */
	public Piece movePiece(BoardPosition from, BoardPosition to)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public void setPieceAt(int i, int j, Piece piece)
	{
		// TODO implement.
		throw new UnsupportedOperationException();
	}

	public static boolean getCoordinateExists(BoardPosition position)
	{
		return Board.getCoordinateExists(position.getI(), position.getJ());
	}

	public static boolean getCoordinateExists(int i, int j)
	{
		return i >= 0 && i < Board.DIMENSION && j >= 0 && j < Board.DIMENSION;
	}

}
