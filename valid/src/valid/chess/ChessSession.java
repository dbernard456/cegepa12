
package valid.chess;

import java.util.Observable;

import valid.chess.board.Board;

public class ChessSession extends Observable
{
	private ChessSessionDelegate delegate;
	private Player whitePlayer;
	private Player blackPlayer;
	private Player currentPlayer;
	private Board currentBoard;

	public ChessSession()
	{
		// TODO implement.

		// Post Conditions
		assert this.currentPlayer == this.whitePlayer;

		assert this.invariant();
		throw new UnsupportedOperationException();
	}

	public void setChessSessionDelegate(ChessSessionDelegate delegate)
	{
		// TODO implement.

		throw new UnsupportedOperationException();
	}

	private boolean invariant()
	{
		boolean success = true;

		success = success && (this.whitePlayer != null);
		success = success && (this.blackPlayer != null);

		success = success && (this.whitePlayer.getColor() == Chess.PLAYER_WHITE);
		success = success && (this.blackPlayer.getColor() == Chess.PLAYER_BLACK);

		success = success && (this.currentBoard != null);
		success = success && (this.currentPlayer != null);

		return success;
	}
}
