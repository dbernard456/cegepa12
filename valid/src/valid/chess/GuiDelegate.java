
package valid.chess;

/**
 * Interface qui décrit la logique des délégés d'interface graphique
 * 
 * @author David Bernard
 * @author Danny Dombrowski
 */
public interface GuiDelegate
{
	/**
	 * called when a new game is requested.
	 */
	public void onNewGameDemanded();

	/**
	 * 
	 */
	public void onExitDemanded();

	/**
	 * 
	 * @param i
	 * @param j
	 */
	public void onBoardClickedAt(int i, int j);

	/**
	 * 
	 * @param i
	 * @param j
	 */
	public void onPieceDragStarted(int i, int j);

	/**
	 * 
	 * @param i
	 * @param j
	 */
	public void onPieceDragEnded(int i, int j);

}
