
package valid.chess;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.SwingUtilities;

import valid.chess.gui.frame.Gui;

/**
 * Classe principale qui contient le controlleur d'échecs et d'interface graphique.
 * 
 * @author David Bernard
 * @author Danny Dombrowski
 * @since Java 1.6
 */
public class Chess
{
	/**
	 * Constante qui représente la couleur du joueur blanc.
	 */
	public static final Color PLAYER_WHITE = Color.WHITE;

	/**
	 * Constante qui représente la couleur du joueur noir.
	 */
	public static final Color PLAYER_BLACK = Color.BLACK;

	private ChessSession chessSession;
	private ChessSessionController controller;
	private ChessGui gui;
	private GuiController guiController;

	/**
	 * Constructeur de la classe Controlleur.
	 */
	public Chess()
	{
		this.controller = new ChessSessionController();
		this.guiController = new GuiController();

		// Post Conditions
		assert this.guiController != null;
		assert this.controller != null;

		assert this.invariant();
	}

	/**
	 * Lance l'interface utilisateur.
	 * 
	 * @throws InterruptedException
	 * @throws InvocationTargetException
	 */
	public void start() throws InterruptedException, InvocationTargetException
	{
		Launcher launcher;

		launcher = new Launcher(this.guiController);

		SwingUtilities.invokeAndWait(launcher);

		this.gui = launcher.getGui();

		this.invariant();

		// Post Conditions
		assert this.chessSession != null;
		assert this.gui != null;
		assert this.gui.isVisible();

		assert this.invariant();
	}

	/**
	 * Termine l'exection du programme.
	 */
	public void exitProgram()
	{
		throw new UnsupportedOperationException();
	}

	private boolean invariant()
	{
		boolean success = true;

		success = success && (this.gui != null);
		success = success && (this.controller != null);

		return success;
	}

	/**
	 * Délégué de la partie métier.
	 * 
	 * @author David Bernard
	 * @author Danny Dombrowski
	 * 
	 */
	private class ChessSessionController implements Observer, ChessSessionDelegate
	{
		@Override
		public void update(Observable o, Object arg)
		{
			// TODO implement.
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Délégué de la partie vue.
	 * 
	 * @author David Bernard
	 * @author Danny Dombrowski
	 * 
	 */
	private class GuiController implements GuiDelegate
	{

		@Override
		public void onNewGameDemanded()
		{
			// TODO Implémenter
			throw new UnsupportedOperationException();
		}

		@Override
		public void onExitDemanded()
		{
			// TODO Implémenter
			throw new UnsupportedOperationException();
		}

		@Override
		public void onBoardClickedAt(int i, int j)
		{
			// TODO Implémenter
			throw new UnsupportedOperationException();
		}

		@Override
		public void onPieceDragStarted(int i, int j)
		{
			// TODO Implémenter
			throw new UnsupportedOperationException();
		}

		@Override
		public void onPieceDragEnded(int i, int j)
		{
			// TODO Implémenter
			throw new UnsupportedOperationException();
		}
	}

	private class Launcher implements Runnable
	{
		private Gui gui;
		private GuiController guiController;

		private Launcher(GuiController guiController)
		{
			this.guiController = guiController;

		}

		@Override
		public void run()
		{
			this.gui = new Gui();
			this.gui.setGuiDelegate(this.guiController);

			this.gui.setVisible(true);
		}

		public Gui getGui()
		{
			return this.gui;
		}
	}

	/**
	 * Valideur de couleur.
	 * 
	 * @param color la couleur a valider
	 * 
	 * @return vrai si valide.
	 */
	public static boolean validateColor(final Color color)
	{
		return (color != null) && ((color == Chess.PLAYER_BLACK) || (color == Chess.PLAYER_WHITE));
	}

	@SuppressWarnings("javadoc")
	public static void main(String... args)
	{
		Chess chess = new Chess();

		try
		{
			chess.start();
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (InvocationTargetException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
