
package valid.chess.piece;

import java.awt.Color;
import java.util.ArrayList;

import valid.chess.Chess;
import valid.chess.board.BoardPosition;

public abstract class Piece
{
	private Color color = null;
	private int moveCount = 0;

	public Piece(Color color)
	{
		// Pre Conditions
		assert (color != null) && ((color == Chess.PLAYER_BLACK) || (color == Chess.PLAYER_WHITE));

		if ((color != null) && ((color != Chess.PLAYER_BLACK) || (color != Chess.PLAYER_WHITE)))
		{
			throw new RuntimeException("Déclaration d'une pièce de couleur invalide");
		}

		this.moveCount = 0;
		this.color = color;

		// Post Conditions
		assert this.moveCount == 0;

		assert this.invariant();

		throw new UnsupportedOperationException();
	}

	public void incrementMove()
	{
		this.moveCount++;
	}

	public int getMoveCount()
	{
		return this.moveCount;
	}

	public Color getColor()
	{
		return this.color;
	}

	private boolean invariant()
	{
		boolean success = true;

		success = success && (this.color != null);
		success = success && Chess.validateColor(this.color);
		success = success && (this.moveCount >= 0);

		return success;
	}

	public abstract ArrayList<BoardPosition> generateAllowedMoves(BoardPosition currentPosition);
}
