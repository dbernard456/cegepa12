
package valid.chess.piece;

import java.awt.Color;
import java.util.ArrayList;

import valid.chess.Chess;
import valid.chess.board.BoardPosition;

public class Pawn extends Piece
{

	public Pawn(Color color)
	{
		super(color);
		// TODO implement.

		// Post Conditions
		// TODO create post conditions

		assert this.invariant();

		throw new UnsupportedOperationException();
	}

	private boolean invariant()
	{
		throw new UnsupportedOperationException();
		// TODO codé l'invatiriant
	}

	@Override
	public ArrayList<BoardPosition> generateAllowedMoves(BoardPosition currentPosition)
	{
		// Pawn est spécial car celui ci ne peut reculer, alors il y a des changement s'il est blanc ou noir
		return (Chess.PLAYER_WHITE == this.getColor()) ? this.generateAllowedMovesWhite(currentPosition) : this.generateAllowedMovesBlack(currentPosition);
	}

	private ArrayList<BoardPosition> generateAllowedMovesWhite(BoardPosition currentPosition)
	{
		/*
		 * ArrayList<BoardPosition> positions = new ArrayList<BoardPosition>();
		 * 
		 * if (currentPosition.getI() <= Board.DIMENSION - 2)
		 * 
		 * return positions;
		 */

		return null;
	}

	private ArrayList<BoardPosition> generateAllowedMovesBlack(BoardPosition currentPosition)
	{
		return null;
	}
}
