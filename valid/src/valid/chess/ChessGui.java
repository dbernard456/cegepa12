
package valid.chess;

public interface ChessGui
{
	public void setGuiDelegate(GuiDelegate delegate);

	public boolean isVisible();
}
